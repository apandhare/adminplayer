<?php
//check for the posted data and decode it
if (isset($_POST["data"]) && ($_POST["data"] !="")){
    $data = $_POST["data"];
    $data = base64_decode($data);
    $im = imagecreatefromstring($data);
}
//make a file name
$filename = $_POST["filename"];

//save the image to the disk
if (isset($im) && $im != false) {
    $imgFile = 'snapshot/'.$filename;

    //delete the file if it already exists
    if(file_exists($imgFile)){
        unlink($imgFile);      
    }

    $result = imagepng($im, $imgFile);
    imagedestroy($im);
    echo "/".$filename;
}
else {
    echo 'Error creating '.$filename;
}
?>