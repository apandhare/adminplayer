package com.videoplayer
{
	public class PlayBackMode
	{
		public static var REWIND_START:String = "rewindStarted";
		public static var REWIND_END:String = "rewindEnded";
		public static var FF_START:String = "fastForwardStarted";
		public static var FF_END:String = "fastForwardEnded";
		
		public static var NORMAL:String = "normalPlayback";
		public static var STEP_BACK:String = "steppedBack";
		public static var STEP_AHEAD:String = "steppedAhead";
		public static var SLOW_MOTION:String = "slowMotion";
		
		public static var TRACK_DRAG:String = "seekPositionChange";
		public static var ZOOM:String = "zoomLevelChange";
		public static var ROTATE:String = "rotationChange";
	}
}