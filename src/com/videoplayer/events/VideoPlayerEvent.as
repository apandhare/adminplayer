package com.videoplayer.events
{
	import flash.events.Event;
	
	import mx.core.IDataRenderer;
	
	public class VideoPlayerEvent extends Event
	{
		public static const LOADING_STATUS:String = "loadingStatus";
		public static const VIDEO_STATUS:String = "videoStatus";
		public static const RECORDING_STATUS:String = "videoRecordingStatus";
		public static const VIDEO_PLAYBACK_COMPLETE:String = "videoPlayBackComplete";
		public static const VIDEO_PLAYBACK_TIME_CHANGE:String = "videoPlayBackCurrentTimeChange";
		public static const TRAVERSE_TIME:String = "traverseToTime";
		
		public function VideoPlayerEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		public var stepBackInterval:Number;
		public var stepAheadInterval:Number;
		public var slowMotionPreset:Number;
		public var playHeadPosition:Number;
		public var playBackMode:String;
		private var _data:Object;
		public function set data(value:Object):void {
			_data = value;
		}
		public function get data():Object {
			return _data;
		}
		
		public var duration:Number;
	}
}