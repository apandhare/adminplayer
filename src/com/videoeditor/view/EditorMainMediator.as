package com.videoeditor.view
{
	import com.videoeditor.As3JsCommunicator;
	import com.videoeditor.EditorOperation;
	import com.videoeditor.model.VideoInfo;
	import com.videoeditor.signals.AnnotationInfoGeneratedSignal;
	import com.videoeditor.signals.CreateVideoObjectSignal;
	import com.videoeditor.signals.ModifyEditedDataSignal;
	import com.videoeditor.signals.SaveEditedDataSignal;
	import com.videoeditor.signals.SaveEditedDataSuccessSignal;
	import com.videoeditor.signals.VideoObjectFormed;
	import com.videoplayer.events.VideoPlayerEvent;
	import com.whiteboard.AppConstants;
	import com.whiteboard.Utility;
	import com.whiteboard.controller.SelectionController;
	import com.whiteboard.events.WhiteboardEvent;
	
	import core.helpers.events.BaseEventDispatcher;
	import core.library.busyWindow.BusyAlert;
	import core.library.logger.CustomLogger;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;
	import flash.utils.setTimeout;
	
	import mx.core.FlexGlobals;
	
	import org.robotlegs.core.IMediator;
	import org.robotlegs.mvcs.Mediator;

	public class EditorMainMediator extends Mediator implements IMediator
	{
		public function EditorMainMediator()
		{
			super();
		}
		
		[Inject]
		public var view:EditorMain;
		
		[Inject]
		public var configVideoObject:CreateVideoObjectSignal;
		
		[Inject]
		public var videoObjectFormed:VideoObjectFormed;
		
		[Inject]
		public var videoModel:VideoInfo;
		
		[Inject]
		public var modifyData:ModifyEditedDataSignal;
		
		[Inject]
		public var saveData:SaveEditedDataSignal;
		
		[Inject]
		public var saveSuccess:SaveEditedDataSuccessSignal;
		
		[Inject]
		public var annotationLstGenerated:AnnotationInfoGeneratedSignal;
		
		//Hardcoded XML that will be passed from PHP to the admin player
		private var vidInfo:XML = <video>
  <videoUrl>assets/myfile_8.mp4</videoUrl>
  <annotations>
    <annotationObject>
      <alpha>1</alpha>
      <imageUrl>http://dev01.rzminc.com/admin player/images/Jellyfish.jpg</imageUrl>
      <displayEndTime>2</displayEndTime>
      <displayStartTime>0</displayStartTime>
      <fillAlpha>1</fillAlpha>
      <fillColor>16777215</fillColor>
      <filters/>
      <finalX>0</finalX>
      <finalY>0</finalY>
      <height>150</height>
      <hexfillColor>#ffffff</hexfillColor>
      <hexlineColor>#00000</hexlineColor>
      <hextextColor>#00000</hextextColor>
      <index>NaN</index>
      <initialX>65</initialX>
      <initialY>16</initialY>
      <lineAlpha>1</lineAlpha>
      <lineColor>0</lineColor>
      <linethickness>2</linethickness>
      <name>imageinstance699</name>
      <oManipulateMove>false</oManipulateMove>
      <source/>
      <radius>0</radius>
      <rotation>NaN</rotation>
      <scaleX>1</scaleX>
      <scaleY>1</scaleY>
      <text/>
      <textAlpha>1</textAlpha>
      <textColor>0</textColor>
      <textFamily>arial</textFamily>
      <textSize>12</textSize>
      <type>image</type>
      <width>150</width>
    </annotationObject>
<annotationObject>
			<alpha>1</alpha>
			<displayEndTime>2.833</displayEndTime>
			<displayStartTime>0.833</displayStartTime>
			<fillAlpha>1</fillAlpha>
			<fillColor>16777215</fillColor>
			<filters/>
			<finalX>0</finalX>
			<finalY>0</finalY>
			<height>55</height>
			<hexfillColor>#ffffff</hexfillColor>
			<hexlineColor>#408080</hexlineColor>
			<hextextColor>#408080</hextextColor>
			<index>NaN</index>
			<initialX>153</initialX>
			<initialY>182</initialY>
			<lineAlpha>1</lineAlpha>
			<lineColor>4227200</lineColor>
			<linethickness>2</linethickness>
			<name>buttonCustomButton541</name>
			<oManipulateMove>false</oManipulateMove>
			<source/>
			<radius>0</radius>
			<rotation>NaN</rotation>
			<scaleX>NaN</scaleX>
			<scaleY>NaN</scaleY>
			<text>Enter Text here</text>
			<textAlpha>1</textAlpha>
			<textColor>4227200</textColor>
			<textFamily>Pacifico</textFamily>
			<textSize>12</textSize>
			<type>button</type>
			<width>135</width>
		  </annotationObject>
		  <annotationObject>
			<alpha>1</alpha>
			<displayEndTime>2.833</displayEndTime>
			<displayStartTime>0.833</displayStartTime>
			<fillAlpha>1</fillAlpha>
			<fillColor>12632256</fillColor>
			<filters/>
			<finalX>0</finalX>
			<finalY>0</finalY>
			<height>42</height>
			<hexfillColor>#c0c0c0</hexfillColor>
			<hexlineColor>#ffffff</hexlineColor>
			<hextextColor>#00000</hextextColor>
			<index>NaN</index>
			<initialX>456</initialX>
			<initialY>292</initialY>
			<lineAlpha>1</lineAlpha>
			<lineColor>16777215</lineColor>
			<linethickness>3</linethickness>
			<name>buttonCustomButton555</name>
			<oManipulateMove>false</oManipulateMove>
			<source/>
			<radius>22</radius>
			<rotation>NaN</rotation>
			<scaleX>NaN</scaleX>
			<scaleY>NaN</scaleY>
			<text>Enter Text here</text>
			<textAlpha>1</textAlpha>
			<textColor>0</textColor>
			<textFamily>arial</textFamily>
			<textSize>12</textSize>
			<type>button</type>
			<width>135</width>
		  </annotationObject>
  </annotations>
</video>;

		//called when the view gets registered with the mediator... automatically called by the robotlegs framework
		//place where we register event listners and signals listener functions
		override public function onRegister():void {
			//this property tells the whiteboard to allow drawn objects to be selected and transformed
			AppConstants.selectable = true; 
			AppConstants.drawingArea = view.board.scribbleArea.area;
			
			/*if(FlexGlobals.topLevelApplication.parameters.hasOwnProperty('videoFile')) {
				view.videoSource = FlexGlobals.topLevelApplication.parameters.videoFile;
			} else {
				view.videoSource = "assets/myfile_8.mp4";
			}*/
			
			CustomLogger.getInstance().sendMessage("Application Initialized. ");
			CustomLogger.getInstance().sendMessage("EditorMainMediator - got registered \n");
			
			BaseEventDispatcher.getInstance().addEventListener(VideoPlayerEvent.LOADING_STATUS, videoLoadingStatus);
			BaseEventDispatcher.getInstance().addEventListener(VideoPlayerEvent.VIDEO_STATUS, videoPlayingStatus);
			BaseEventDispatcher.getInstance().addEventListener(VideoPlayerEvent.VIDEO_PLAYBACK_COMPLETE, onVideoPlaybackComplete);
			BaseEventDispatcher.getInstance().addEventListener(VideoPlayerEvent.VIDEO_PLAYBACK_TIME_CHANGE, onVideoPlaybackTimeChange);
			BaseEventDispatcher.getInstance().addEventListener(WhiteboardEvent.DRAW_INITIATED, triggeredDrawing);
			BaseEventDispatcher.getInstance().addEventListener(WhiteboardEvent.DRAW_COMPLETE, objectDrawn);
			BaseEventDispatcher.getInstance().addEventListener(WhiteboardEvent.DELETE_OBJECT, objectDeleted);
			BaseEventDispatcher.getInstance().addEventListener(WhiteboardEvent.DELETE_ALL_OBJECTS, onDeleteAllObjects);
		
			BaseEventDispatcher.getInstance().addEventListener(WhiteboardEvent.TOOL_SELECTED, onToolSelection);
			BaseEventDispatcher.getInstance().addEventListener(WhiteboardEvent.OBJECT_SELECTED, onObjectSelection);
			BaseEventDispatcher.getInstance().addEventListener("FINAL_SAVE", onTriggerSave);
			BaseEventDispatcher.getInstance().addEventListener("INTERMEDIATE_SAVE", formSaveData);
			
			saveSuccess.add(onXMLSaveSuccess);
			annotationLstGenerated.add(onListGenerated);
			
			view.saveBtn.addEventListener(MouseEvent.CLICK, onTriggerSave);
			As3JsCommunicator.initiateCommunicator(view);
			
			videoObjectFormed.add(onObjectFormation);
			
			setTimeout(configVideoObject.dispatch,500, view.board,vidInfo);
		}
		
		//called once the xml data is successfully transfered to JS
		private function onXMLSaveSuccess():void {
			CustomLogger.getInstance().sendMessage("XML saved    *****     ");
			As3JsCommunicator.doSave(videoModel.originalXML.toString());
		}
		
		//called everytime there is a change in the object details, and transfers the relevant data to JS
		private function onListGenerated():void {
			CustomLogger.getInstance().sendMessage("EditorMainMediator - Annotation List data generated");
			As3JsCommunicator.setAnnotationList(videoModel.annotationsInfo.source);
		}
		
		//triggered every time a new tool type is selected
		private function onToolSelection(evt:WhiteboardEvent):void {
//			CustomLogger.getInstance().sendMessage("Value Changed.    *****     " + evt.data.type);
		}
		
		//triggered everytime a object like button or image is selected which is drawn on top of the video
		private function onObjectSelection(evt:WhiteboardEvent):void {
			view.player.pausePlayBack();
			As3JsCommunicator.setSelectionProperties(evt.data);
		}
		
		//triggered once the recieved XML is tranformed to objects for internal use
		private function onObjectFormation():void {
			view.videoSource = videoModel.videoUrl;
		}
		
		//Video Player event listeners
		//triggered every time the loading of the video completes
		private function videoLoadingStatus(event:VideoPlayerEvent):void {
			CustomLogger.getInstance().sendMessage("Disable / Enable Whiteboard and controls based on VIDEO loading status   " + event.data + "     " + event.duration);
			CustomLogger.getInstance().sendMessage(Utility.convertSecondsToHHMMSS(event.duration));
			As3JsCommunicator.setMaxtimeValue(event.duration);
			BusyAlert.hide();
		}
		
		//triggered everytime the video playback is complete
		private function onVideoPlaybackComplete(event:VideoPlayerEvent):void {
			CustomLogger.getInstance().sendMessage("Video playback got complete.....     ");
			
		}
		
		//logic to display objects for specific intervals... triggered every time interval during the video playback
		private function onVideoPlaybackTimeChange(event:VideoPlayerEvent):void {
			CustomLogger.getInstance().sendMessage("Time CHANGED     :"+event.data);
			var objEvt:WhiteboardEvent = new WhiteboardEvent(WhiteboardEvent.DISPLAY_OBJECT);
			objEvt.data = event.data;
			BaseEventDispatcher.getInstance().dispatchEvent(objEvt);
		}
		
		//triggered everytime there is a change in the video playing status
		private function videoPlayingStatus(event:VideoPlayerEvent):void {
//			CustomLogger.getInstance().sendMessage("\nVideo playing status. ----------    "+event.data);
			if(event.data) {
				SelectionController.getInstance().tool.target = null;
			}
		}
		
		
		
		//Whiteboard event listeners
		//triggered everytime any object is created on the stage
		private function triggeredDrawing(event:WhiteboardEvent):void {
			view.player.pausePlayBack();
			
			CustomLogger.getInstance().sendMessage("Pause the video before drawing object and start timer to record the pause duration   "+videoModel.data.position.length());
		}
		
		//triggered evertime the object creation is complete
		private function objectDrawn(event:WhiteboardEvent):void {
			modifyData.dispatch(EditorOperation.OBJECT_DRAWN, view.player.getPlayheadPosition(), event.data);
		}
		
		//triggered when a object is deleted from the stage
		private function objectDeleted(event:WhiteboardEvent):void {
			modifyData.dispatch(EditorOperation.DELETE, view.player.getPlayheadPosition(), event.data);
		}
		
		//triggered when all the object on the stage are deleted
		private function onDeleteAllObjects(event:WhiteboardEvent):void {
//			if(isRecording) {
//				modifyData.dispatch(EditorOperation.DELETE_ALL, view.player.getPlayStatus(), view.player.getPlayheadPosition(), '', event.data);
//			}
		}		
		
		//dispatches a signal to save the xml data based on the objects drawn on the stage
		private function onTriggerSave(evt:Event):void {
			CustomLogger.getInstance().sendMessage("INITIATE SAVE - SIGNAL DISPATCHED");
			saveData.dispatch(view.board.scribbleArea.area, true);
		}
		
		//dispatches a signal to generate the list of objects data currently on the stage
		private function formSaveData(evt:Event):void {
			CustomLogger.getInstance().sendMessage("FORM ANNOTATION COLLECTION - SIGNAL DISPATCHED");
			saveData.dispatch(view.board.scribbleArea.area, false);
		}
	}
}