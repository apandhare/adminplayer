package com.videoeditor
{
	import com.library.utility.transform.TransformTool;
	import com.videoeditor.view.EditorMain;
	import com.videoplayer.VideoPlayer;
	import com.videoplayer.events.VideoPlayerEvent;
	import com.whiteboard.AppConstants;
	import com.whiteboard.Properties;
	import com.whiteboard.Utility;
	import com.whiteboard.controller.SelectionController;
	import com.whiteboard.events.WhiteboardEvent;
	import com.whiteboard.model.vo.ObjectVO;
	
	import core.helpers.events.BaseEventDispatcher;
	import core.library.logger.CustomLogger;
	
	import flash.external.ExternalInterface;
	
	import mx.controls.Alert;

	public class As3JsCommunicator
	{
		private static var view:EditorMain;
		public static function initiateCommunicator(viewRef)
		{
			view = viewRef;
			
			// call flash function 'addVideo' from js
			ExternalInterface.addCallback("addNewButton", addNewButton);
			ExternalInterface.addCallback("addNewImage", addNewImage);
			
			ExternalInterface.addCallback("setFillColor", setFillColor);
			ExternalInterface.addCallback("setBorderColor", setBorderColor);
			ExternalInterface.addCallback("setTextColor", setTextColor);
			ExternalInterface.addCallback("setFillAlpha", setFillAlpha);
			ExternalInterface.addCallback("setBorderAlpha", setBorderAlpha);
			ExternalInterface.addCallback("setTextAlpha", setTextAlpha);
			ExternalInterface.addCallback("setBorderThickness", setBorderThickness);
			ExternalInterface.addCallback("setFontSize", setFontSize);
			ExternalInterface.addCallback("setFontFamily", setFontFamily);
			ExternalInterface.addCallback("setAlpha", setAlpha);
			ExternalInterface.addCallback("setCurve", setCurve);
			
			ExternalInterface.addCallback("triggerSave", triggerSave);
			
			ExternalInterface.addCallback("deleteSelection", deleteSelection);
			ExternalInterface.addCallback("setObjectSelection", setObjectSelection);
			ExternalInterface.addCallback("setStartTime", setStartTime);
			ExternalInterface.addCallback("setEndTime", setEndTime);
		}
		
		public static function setMaxtimeValue(maxVal):void {
//			var value:Number = Math.floor(maxVal * 10)/10;
			ExternalInterface.call("setMaxValue", maxVal);
		}
	
		private static function deleteSelection():void {
			CustomLogger.getInstance().sendMessage("DELETE Selected object    " + SelectionController.getInstance().selectedInstance);
			var evtObj:WhiteboardEvent = new WhiteboardEvent(WhiteboardEvent.DELETE_OBJECT);
			evtObj.data = SelectionController.getInstance().selectedInstance;
			BaseEventDispatcher.getInstance().dispatchEvent(evtObj);
		}
		
		private static function setObjectSelection(name:String):void {
			for(var i=0;i<AppConstants.drawingArea.numChildren;i++) {
				var child = AppConstants.drawingArea.getChildAt(i);
				if(child is TransformTool) {
					
				} else {
					CustomLogger.getInstance().sendMessage("---         " + child);
					if(child._model.name == name) {
						SelectionController.getInstance().selectedInstance = child;
						var evtObj:VideoPlayerEvent = new VideoPlayerEvent(VideoPlayerEvent.TRAVERSE_TIME);
						evtObj.duration = child._model.displayStartTime;
						BaseEventDispatcher.getInstance().dispatchEvent(evtObj);
						
					}
				}
			}
		}
		
		public static function setAnnotationList(data) {
			CustomLogger.getInstance().sendMessage("CALLING JS FUNCTION NOW");
			ExternalInterface.call("setListData", data);
		}
		
		public static function doSave(data) {
			CustomLogger.getInstance().sendMessage("CALLING JS FUNCTION NOW");
			ExternalInterface.call("onSaveInitiate", data);
		}
		private static function triggerSave() {
			CustomLogger.getInstance().sendMessage("INITIATE SAVE NOW");
			BaseEventDispatcher.getInstance().dispatchEvent(new WhiteboardEvent("FINAL_SAVE"));
		}
		
		private static function addNewButton():void {
			BaseEventDispatcher.getInstance().dispatchEvent(new WhiteboardEvent(WhiteboardEvent.ADD_BUTTON));
		}
		
		private static function addNewImage(url:String):void {
			Properties.imageUrl = url;
			BaseEventDispatcher.getInstance().dispatchEvent(new WhiteboardEvent(WhiteboardEvent.ADD_IMAGE));
		}
		
		public static function setSelectionProperties(info:Object):void {
			CustomLogger.getInstance().sendMessage(int(info.lineColor).toString(16));
			var col:String = int(info.lineColor).toString(16);
			if(col.length == 6) {
				info.hexlineColor = '#' + col;
			} else if(col.length == 4) {
				info.hexlineColor = '#' + '00' + col;
			} else {
				info.hexlineColor = '#' + '0000' + col;
			}
			
			var col:String = int(info.fillColor).toString(16);
			if(col.length == 6) {
				info.hexfillColor = '#' + col;
			} else if(col.length == 4) {
				info.hexfillColor = '#' + '00' + col;
			} else {
				info.hexfillColor = '#' + '0000' + col;
			}
			
			var col:String = int(info.textColor).toString(16);
			if(col.length == 6) {
				info.hextextColor = '#' + col;
			} else if(col.length == 4) {
				info.hextextColor = '#' + '00' + col;
			} else {
				info.hextextColor = '#' + '0000' + col;
			}
//			info.hexlineColor = int(info.lineColor).toString(16);
//			info.hexfillColor = int(info.fillColor).toString(16);
//			info.hextextColor = int(info.textColor).toString(16);
			if(!info.hasOwnProperty('radius'))
				info.radius = 0;
			
//			info.displayStartTime = Math.floor(info.displayStartTime * 10)/10;
//			info.displayEndTime = Math.floor(info.displayEndTime * 10)/10;
			
			ExternalInterface.call("setSelectionProperties", info);
		}
		
		private static function setFillColor(color:String):void {
			CustomLogger.getInstance().sendMessage(color.replace('#','0x') + "    FILL COLOR");
//			Properties.selFillColor = color;
			SelectionController.getInstance().selectedInstance.setFillColor(color.replace('#','0x'));
		}
		private static function setBorderColor(color:String):void {
			CustomLogger.getInstance().sendMessage(color.replace('#','0x') + "    BORDER COLOR");
//			Properties.selLineColor = color;
			SelectionController.getInstance().selectedInstance.setBorderColor(color.replace('#','0x'));
		}
		private static function setTextColor(color:String):void {
			CustomLogger.getInstance().sendMessage(color.replace('#','0x') + "    TEXT COLOR");
//			Properties.selTextColor = color;
			SelectionController.getInstance().selectedInstance.setTextColor(color.replace('#','0x'));
		}
		private static function setFillAlpha(alpha:Number):void {
			CustomLogger.getInstance().sendMessage(alpha + "    FILL ALPHA");
//			Properties.selFillAlpha = alpha;
			SelectionController.getInstance().selectedInstance.setFillAlpha(alpha);
		}
		private static function setBorderAlpha(alpha:Number):void {
			CustomLogger.getInstance().sendMessage(alpha + "    BORDER ALPHA");
//			Properties.selLineAlpha = alpha;
			SelectionController.getInstance().selectedInstance.setBorderAlpha(alpha);
		}
		private static function setTextAlpha(alpha:Number):void {
			CustomLogger.getInstance().sendMessage(alpha + "    TEXT ALPHA");
//			Properties.textAlpha = alpha;
			SelectionController.getInstance().selectedInstance.setTextAlpha(alpha);
		}
		private static function setBorderThickness(thickness:Number):void {
			CustomLogger.getInstance().sendMessage(thickness + "    LINE THICKNESS");
//			Properties.selLineThickness = thickness;
			SelectionController.getInstance().selectedInstance.setBorderThickness(thickness);
		}
		private static function setFontSize(size:Number):void {
			CustomLogger.getInstance().sendMessage(size + "    FONT SIZE");
//			Properties.textSize = size;
			SelectionController.getInstance().selectedInstance.setFontSize(size);
		}
		private static function setFontFamily(family:String):void {
			CustomLogger.getInstance().sendMessage(family + "    FONT FAMILY");
//			Properties.selFont = family;
			SelectionController.getInstance().selectedInstance.setFontFamily(family);
		}
		private static function setAlpha(alpha:Number):void {
			CustomLogger.getInstance().sendMessage(alpha + "    ALPHA");
//			Properties.alpha = alpha;
			SelectionController.getInstance().selectedInstance.setAlpha(alpha);
		}
		private static function setCurve(radius:Number):void {
			CustomLogger.getInstance().sendMessage(radius + "    CORNER RADIUS");
			SelectionController.getInstance().selectedInstance.cornerRad = radius;
		}
		
		private static function setStartTime(value:Number):void {
			CustomLogger.getInstance().sendMessage(value + "    START TIME");
			SelectionController.getInstance().selectedInstance._model.displayStartTime = value;
			SelectionController.getInstance().selectedInstance._model.startTime = Utility.convertSecondsToHHMMSS(value);

			BaseEventDispatcher.getInstance().dispatchEvent(new WhiteboardEvent("INTERMEDIATE_SAVE"));
			
			var objEvt:WhiteboardEvent = new WhiteboardEvent(WhiteboardEvent.DISPLAY_OBJECT);
			objEvt.data = view.player.getCurrentPauseTime();
			BaseEventDispatcher.getInstance().dispatchEvent(objEvt);
			
			SelectionController.getInstance().tool.target = null;
		}
		private static function setEndTime(value:Number):void {
			CustomLogger.getInstance().sendMessage(value + "    END TIME");
			SelectionController.getInstance().selectedInstance._model.displayEndTime = value;
			SelectionController.getInstance().selectedInstance._model.endTime = Utility.convertSecondsToHHMMSS(value);
			
			BaseEventDispatcher.getInstance().dispatchEvent(new WhiteboardEvent("INTERMEDIATE_SAVE"));
		}
		
	}
}