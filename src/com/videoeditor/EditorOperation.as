package com.videoeditor
{
	public class EditorOperation
	{
		public static var OBJECT_DRAWN:int = 1;
		public static var DELETE:int = 2;
		public static var DELETE_ALL:int = 4;
	}
}