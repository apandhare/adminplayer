package com.videoeditor
{
	import com.videoeditor.business.EditorAppDelegate;
	import com.videoeditor.business.IEditorAppDelegate;
	import com.videoeditor.controller.CreateVideoObjectCommand;
	import com.videoeditor.controller.ModifyEditedDataCommand;
	import com.videoeditor.controller.SaveVideoInfoCommand;
	import com.videoeditor.model.VideoInfo;
	import com.videoeditor.signals.AnnotationInfoGeneratedSignal;
	import com.videoeditor.signals.CreateVideoObjectSignal;
	import com.videoeditor.signals.ModifyEditedDataSignal;
	import com.videoeditor.signals.SaveEditedDataSignal;
	import com.videoeditor.signals.SaveEditedDataSuccessSignal;
	import com.videoeditor.signals.VideoObjectFormed;
	import com.videoeditor.view.EditorMain;
	import com.videoeditor.view.EditorMainMediator;
	import com.whiteboard.business.AppDelegate;
	import com.whiteboard.business.IAppDelegate;
	import com.whiteboard.controller.BoardMouseDownCommand;
	import com.whiteboard.controller.GetConfigDataCommand;
	import com.whiteboard.model.ConfigModel;
	import com.whiteboard.model.SelectedToolModel;
	import com.whiteboard.signals.BoardMouseDownSignal;
	import com.whiteboard.signals.ConfigLoadFailSignal;
	import com.whiteboard.signals.ConfigLoadSuccessSignal;
	import com.whiteboard.signals.GetConfigDataSignal;
	import com.whiteboard.view.*;
	
	import core.helpers.events.BaseEventDispatcher;
	import core.library.logger.CustomLogger;
	
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	
	import org.robotlegs.base.ContextEvent;
	import org.robotlegs.core.ISignalContext;
	import org.robotlegs.mvcs.SignalContext;
	
	public class AppContext extends SignalContext implements ISignalContext
	{
		public function AppContext(contextView:DisplayObjectContainer=null, autoStartup:Boolean=true)
		{
			super(contextView, autoStartup);
		}
		
		override public function startup():void {
			CustomLogger.getInstance().launch = true;
			CustomLogger.getInstance().sendMessage("VideoEditor -- Bootstrap begins here");
			
			//whiteboard mappings
			//map the models here
			injector.mapSingleton(ConfigModel);
			injector.mapSingleton(SelectedToolModel);
			
			//map the signals which do not relate to any command
			injector.mapSingleton(ConfigLoadSuccessSignal);
			injector.mapSingleton(ConfigLoadFailSignal);
			
			//map the services here
			injector.mapSingletonOf( IAppDelegate, AppDelegate );
			
			//map the events and command classes here
			signalCommandMap.mapSignalClass(GetConfigDataSignal, GetConfigDataCommand);
			signalCommandMap.mapSignalClass(BoardMouseDownSignal, BoardMouseDownCommand);
			
			//map the view and respective mediators here
			mediatorMap.mapView(WhiteBoardMain, WhiteBoardMainMediator);
			mediatorMap.mapView(ContentArea, ContentAreaMediator);
			
			
			
			//map the models here
			injector.mapSingleton(VideoInfo);
			
			//map the signals which do not relate to any command
			injector.mapSingleton(VideoObjectFormed);
			injector.mapSingleton(SaveEditedDataSuccessSignal);
			injector.mapSingleton(AnnotationInfoGeneratedSignal);
			
			//map the services here
			injector.mapSingletonOf( IEditorAppDelegate, EditorAppDelegate );
			
			//map the events and command classes here
			signalCommandMap.mapSignalClass(CreateVideoObjectSignal, CreateVideoObjectCommand);
			signalCommandMap.mapSignalClass(ModifyEditedDataSignal, ModifyEditedDataCommand);
			signalCommandMap.mapSignalClass(SaveEditedDataSignal, SaveVideoInfoCommand);
			
			//map the view and respective mediators here
			mediatorMap.mapView(EditorMain, EditorMainMediator);
		}
	}
}