package com.videoeditor
{
	public class Modes
	{
		public static var PLAYER_MODE:String = "player";
		public static var PLAYER_VIEW_MODE:String = "playerWithoutControls";
		public static var PREVIEW_MODE:String = "preview";
		public static var EDITOR_MODE:String = "editor";
	}
}