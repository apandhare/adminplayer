package com.videoeditor.controller
{
	import com.videoeditor.business.IEditorAppDelegate;
	import com.videoeditor.model.VideoInfo;
	import com.videoeditor.signals.AnnotationInfoGeneratedSignal;
	import com.videoeditor.signals.VideoObjectFormed;
	import com.whiteboard.Utility;
	import com.whiteboard.controller.SelectionController;
	import com.whiteboard.events.WhiteboardEvent;
	import com.whiteboard.model.vo.ObjectVO;
	import com.whiteboard.view.WhiteBoardMain;
	
	import core.helpers.events.BaseEventDispatcher;
	import core.library.busyWindow.BusyAlert;
	import core.library.logger.CustomLogger;
	
	import flash.events.Event;
	import flash.geom.Point;
	import flash.xml.XMLDocument;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.managers.CursorManager;
	import mx.rpc.IResponder;
	import mx.rpc.xml.SimpleXMLDecoder;
	import mx.utils.ObjectUtil;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	public class CreateVideoObjectCommand extends SignalCommand implements IResponder
	{
		[Inject]
		public var videoModel:VideoInfo;
		
		[Inject]
		public var fetchSuccess:VideoObjectFormed;
		
		[Inject]
		public var videoXML:Object;
		
		[Inject]
		public var annotationLstGenerated:AnnotationInfoGeneratedSignal;
		
		[Inject]
		public var board:WhiteBoardMain;
		
		public function CreateVideoObjectCommand()
		{
			super();
		}
		
		override public function execute():void
		{
			CustomLogger.getInstance().sendMessage("CreateVideoObjectCommand - execute method entered    ");
			videoModel.originalXML = new XML(videoXML);
			videoModel.videoUrl = videoXML.videoUrl;
			
			fetchSuccess.dispatch();
			
			videoModel.annotationsInfo = new ArrayCollection();
			
			//logic to draw objects based on the XML data recieved
			var shapes:XMLList = videoXML.descendants('annotationObject');
			if(shapes.length() > 0) {
	//			CustomLogger.getInstance().sendMessage(shapes.toString());
				for(var i=0;i<shapes.length();i++) {
					var infoObject:ObjectVO = getObjectformat(new XMLDocument(shapes[i].toString()));
					
					if(!infoObject.text)
						infoObject.text = "";
					//delete following lines of code
					infoObject.startTime = Utility.convertSecondsToHHMMSS(infoObject.displayStartTime);
					infoObject.endTime = Utility.convertSecondsToHHMMSS(infoObject.displayEndTime);
					//-------------------- till here
					
					videoModel.annotationsInfo.addItem(infoObject);
					board.drawShape(infoObject, true);
				} 
				
				//turn off the visibility of all the added objects
				BaseEventDispatcher.getInstance().dispatchEvent(new Event("DISPLAY_OFF"));
				
				//display objects at the start of the video
				var objEvt:WhiteboardEvent = new WhiteboardEvent(WhiteboardEvent.DISPLAY_OBJECT);
				objEvt.data = 0;
				BaseEventDispatcher.getInstance().dispatchEvent(objEvt);
				
				//set selection to null
				SelectionController.getInstance().selectedInstance = null;
			}

			if(videoModel.annotationsInfo.length > 0)
				annotationLstGenerated.dispatch();
		}
		
		// function that returns a ObjectVO representation of the XML being provided
		public function getObjectformat(xmlData:XMLDocument):ObjectVO {
			var infoObject:ObjectVO = new ObjectVO();
			var decoder:SimpleXMLDecoder = new SimpleXMLDecoder(true);
			var obj:Object = decoder.decodeXML(xmlData);
//			CustomLogger.getInstance().sendMessage("Creating xml object " + obj.annotationObject);
			for(var propName:String in obj.annotationObject) {
//				CustomLogger.getInstance().sendMessage(propName + " = " + obj.annotationObject[propName]);
				if(propName == 'source' && obj.annotationObject[propName]) {
					infoObject.pointsCollection = new ArrayCollection();
					//					CustomLogger.getInstance().sendMessage(obj.properties[propName].item.length);
					for(var i=0; i<obj.annotationObject[propName].item.length;i++) {
						//						CustomLogger.getInstance().sendMessage(obj.properties[propName].item[i].start.x + "       " + obj.properties[propName].item[i].start.y);
						//						CustomLogger.getInstance().sendMessage(obj.properties[propName].item[i].end.x + "       " + obj.properties[propName].item[i].end.y);
						var startingPt:Point = new Point(obj.annotationObject[propName].item[i].start.x, obj.annotationObject[propName].item[i].start.y);
						var endingPt:Point = new Point(obj.annotationObject[propName].item[i].end.x, obj.annotationObject[propName].item[i].end.y);
						
						infoObject.pointsCollection.addItem({start:startingPt, end:endingPt});
					}
				} else {
//					if((obj.annotationObject as Object).hasOwnProperty(propName))
					infoObject[propName] = obj.annotationObject[propName];
				}
			}
			return infoObject;
		}
		
		public function result(data:Object):void
		{
	
		}
		
		public function fault(info:Object):void
		{

		}
	}
}