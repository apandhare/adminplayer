package com.videoeditor.controller
{
	import com.library.utility.transform.TransformTool;
	import com.videoeditor.business.IEditorAppDelegate;
	import com.videoeditor.model.VideoInfo;
	import com.videoeditor.signals.AnnotationInfoGeneratedSignal;
	import com.videoeditor.signals.SaveEditedDataSuccessSignal;
	
	import core.library.busyWindow.BusyAlert;
	import core.library.logger.CustomLogger;
	
	import flash.net.FileReference;
	import flash.utils.ByteArray;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.core.UIComponent;
	import mx.managers.CursorManager;
	import mx.rpc.IResponder;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	public class SaveVideoInfoCommand extends SignalCommand implements IResponder
	{
		[Inject]
		public var service:IEditorAppDelegate;
		
		[Inject]
		public var videoModel:VideoInfo;
		
		[Inject]
		public var saveSuccess:SaveEditedDataSuccessSignal;
		
		[Inject]
		public var annotationLstGenerated:AnnotationInfoGeneratedSignal;
		
		[Inject]
		public var drawingArea:UIComponent;
		
		[Inject]
		public var isFinalSave:Boolean;
		
		public function SaveVideoInfoCommand()
		{
			super();
		}
		
		override public function execute():void
		{
			CustomLogger.getInstance().sendMessage("SaveVideoInfoCommand - execute method entered    " + drawingArea.numChildren);
			
			videoModel.data = <annotations/>;
			if(!isFinalSave) 
				videoModel.annotationsInfo = new ArrayCollection();
				
			for(var i=0;i<drawingArea.numChildren;i++) {
				var child = drawingArea.getChildAt(i);
				if(child is TransformTool) {
					
				} else {
//					CustomLogger.getInstance().sendMessage("---         " + child);
					var childData = child._model.getXMLformat();
					videoModel.data.appendChild(childData);
					
					if(!isFinalSave) {
						videoModel.annotationsInfo.addItem(child._model);
					} else {
						//trigger objects to image save on the server now, for all objects except type 'image'
						if(child._model.type != 'image') {
							var objSend:Object = new Object;
							objSend.data = child.snapshotAsBase64(1000,786);
							objSend.filename = child._model.type +"_"+ child._model.name +".png";
							
							service.sendImageDataForSave(this, objSend);
//							var fr:FileReference = new FileReference();
//							fr.save(child.snapshotAsByteArray(1000,786));
						}
					}
				}
			}
			
			
			if(isFinalSave) {
				CustomLogger.getInstance().sendMessage("elements details --   \n " + videoModel.data);
				//replace the original XML with the fresh data generated ....
				videoModel.originalXML.annotations = videoModel.data;
	//			CustomLogger.getInstance().sendMessage("Final elements details --    " + videoModel.originalXML);
				
				saveSuccess.dispatch();
			} else {
				annotationLstGenerated.dispatch();
			}
		}
		
		public function result(data:Object):void
		{
			CustomLogger.getInstance().sendMessage("\n\n" + data.result);
		}
		
		public function fault(info:Object):void
		{
			CustomLogger.getInstance().sendMessage("\n\n Failed to save list of images. Please try again later."  );
		}
	}
}