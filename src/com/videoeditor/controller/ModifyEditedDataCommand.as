package com.videoeditor.controller
{
	import com.videoeditor.EditorOperation;
	import com.videoeditor.business.IEditorAppDelegate;
	import com.videoeditor.events.EditorEvent;
	import com.videoeditor.model.VideoInfo;
	import com.whiteboard.Utility;
	import com.whiteboard.events.WhiteboardEvent;
	import com.whiteboard.model.vo.ObjectVO;
	
	import core.helpers.events.BaseEventDispatcher;
	import core.library.logger.CustomLogger;
	
	import flash.utils.getTimer;
	import flash.xml.XMLNode;
	
	import mx.controls.Alert;
	import mx.rpc.IResponder;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	public class ModifyEditedDataCommand extends SignalCommand
	{
		[Inject]
		public var service:IEditorAppDelegate;
		
		[Inject]
		public var operation:int;
		
		[Inject]
		public var videoModel:VideoInfo;
		
		[Inject]
		public var playHeadPosition:Number;
		
		[Inject]
		public var objectInfo:Object;
		
		public function ModifyEditedDataCommand()
		{
			super();
		}
		
		override public function execute():void
		{
			CustomLogger.getInstance().sendMessage("ModifyEditedDataCommand - execute method entered    ");
			switch(operation) {
				
				case EditorOperation.OBJECT_DRAWN:
					recordDrawingDetails();
					break;
				
				case EditorOperation.DELETE:
					deleteDrawingDetails();
					break;
				
				case EditorOperation.DELETE_ALL:
					deleteAllDrawingDetails();
					break;
			}
			
			
		}
		
		private function recordDrawingDetails():void {
			CustomLogger.getInstance().sendMessage("Added/Edited the object details at the pause interval");
			if(objectInfo.hasOwnProperty('displayStartTime') && objectInfo.hasOwnProperty('displayEndTime')) {
				
			} else {
				objectInfo.displayStartTime = playHeadPosition;
				objectInfo.startTime = Utility.convertSecondsToHHMMSS(playHeadPosition);
			
				objectInfo.displayEndTime = playHeadPosition + 2;
				objectInfo.endTime = Utility.convertSecondsToHHMMSS(objectInfo.displayEndTime);
				
//				CustomLogger.getInstance().sendMessage("TESTING     " + objectInfo.getXMLformat());
				CustomLogger.getInstance().sendMessage("TESTING  --- TIME MODEL FINALLY READY   ");
				
				var objEvt:EditorEvent = new EditorEvent(EditorEvent.TIME_MODEL_READY);
				objEvt.data = objectInfo;
				BaseEventDispatcher.getInstance().dispatchEvent(objEvt);
			}
			BaseEventDispatcher.getInstance().dispatchEvent(new WhiteboardEvent("INTERMEDIATE_SAVE"));
		}
		
		private function deleteDrawingDetails():void {
			CustomLogger.getInstance().sendMessage("Remove the deleted object details at the pause interval");
		}
		
		private function deleteAllDrawingDetails():void {
			CustomLogger.getInstance().sendMessage("Clear the recorded data at the pause interval.");
//			videoModel.data = <videoData/>;
//			CustomLogger.getInstance().sendMessage(videoModel.data.toString());
		}
	}
}