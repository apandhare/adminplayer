package com.videoeditor.business
{
	import core.helpers.serviceUtils.ServiceBase;
	import core.library.logger.CustomLogger;
	
	import flash.net.FileReference;
	import flash.utils.ByteArray;
	
	import mx.core.FlexGlobals;
	import mx.rpc.AsyncToken;
	import mx.rpc.IResponder;
	import mx.rpc.http.HTTPService;
	import mx.rpc.remoting.RemoteObject;
	import mx.utils.Base64Encoder;
	import mx.utils.XMLUtil;

	public class EditorAppDelegate implements IEditorAppDelegate
	{
		private var service:HTTPService;
		private var remoteService:RemoteObject;
		
		public function getVideoData(responder:IResponder, videoId:Object):void {
//			remoteService = ServiceBase.getInstance().getRemoteObject('remoteService');
//			var token:AsyncToken = remoteService.getVideoData(videoId);
//			token.addResponder(responder);
			var infoObject:Object = new Object();
			infoObject.videoId = videoId;
			
			service = ServiceBase.getInstance().getHTTPService('editorService');
			service.url = FlexGlobals.topLevelApplication.parameters.fetchUrl;			
			service.method = "GET";
			
			var token:AsyncToken = service.send(infoObject);
			token.addResponder(responder);
		}
		
		public function saveEditedVideoData(responder:IResponder, videoId:Object, editedData:XML):void {
			service = ServiceBase.getInstance().getHTTPService('editorService');
			service.url = FlexGlobals.topLevelApplication.parameters.saveUrl;
			service.method = "POST";
//			var token:AsyncToken = service.send(infoObject);
//			token.addResponder(responder);
		}
		
		public function sendImageDataForSave(responder:IResponder, data:Object):void {
			service = ServiceBase.getInstance().getHTTPService('editorService');
			service.url = "http://dev01.rzminc.com/admin player/saveImage.php";
			var token:AsyncToken = service.send(data);
			token.addResponder(responder);
		}
		
	}
}