package com.videoeditor.business
{
	import flash.utils.ByteArray;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.IResponder;

	public interface IEditorAppDelegate
	{
		function getVideoData(responder:IResponder, videoId:Object):void;
		function saveEditedVideoData(responder:IResponder, videoId:Object, editedData:XML):void;
		function sendImageDataForSave(responder:IResponder, data:Object):void;
	}
}