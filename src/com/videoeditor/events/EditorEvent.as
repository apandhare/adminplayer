package com.videoeditor.events
{
	import flash.events.Event;
	
	import mx.core.IDataRenderer;
	
	public class EditorEvent extends Event
	{
		
		public static const TIME_MODEL_READY:String = "timeBasedModelReady";
		
		public function EditorEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		private var _data:Object;
		public function set data(value:Object):void {
			_data = value;
		}
		public function get data():Object {
			return _data;
		}
		
	}
}