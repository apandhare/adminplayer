package com.videoeditor.signals
{
	import mx.core.UIComponent;
	
	import org.osflash.signals.ISignal;
	import org.osflash.signals.Signal;
	
	public class SaveEditedDataSignal extends Signal implements ISignal
	{
		public function SaveEditedDataSignal()
		{
			super(UIComponent,Boolean);
		}
	}
}