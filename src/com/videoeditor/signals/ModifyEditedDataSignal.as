package com.videoeditor.signals
{
	import org.osflash.signals.ISignal;
	import org.osflash.signals.Signal;
	
	public class ModifyEditedDataSignal extends Signal implements ISignal
	{
		public function ModifyEditedDataSignal()
		{
			super(int, Number, Object);
		}
	}
}