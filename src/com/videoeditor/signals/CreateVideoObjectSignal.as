package com.videoeditor.signals
{
	import com.whiteboard.view.WhiteBoardMain;
	
	import org.osflash.signals.ISignal;
	import org.osflash.signals.Signal;
	
	public class CreateVideoObjectSignal extends Signal implements ISignal
	{
		public function CreateVideoObjectSignal()
		{
			super(WhiteBoardMain, Object);
		}
	}
}