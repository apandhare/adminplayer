package com.videoeditor.signals
{
	import org.osflash.signals.ISignal;
	import org.osflash.signals.Signal;
	
	public class VideoObjectFormed extends Signal implements ISignal
	{
		public function VideoObjectFormed()
		{
			super();
		}
	}
}