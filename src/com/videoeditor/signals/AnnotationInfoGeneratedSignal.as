package com.videoeditor.signals
{
	import mx.core.UIComponent;
	
	import org.osflash.signals.ISignal;
	import org.osflash.signals.Signal;
	
	public class AnnotationInfoGeneratedSignal extends Signal implements ISignal
	{
		public function AnnotationInfoGeneratedSignal()
		{
			super();
		}
	}
}