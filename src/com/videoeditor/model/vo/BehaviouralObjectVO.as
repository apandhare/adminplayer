package com.videoeditor.model.vo
{
	import com.whiteboard.model.vo.ObjectVO;
	
	import mx.utils.ObjectUtil;

	[Bindable]
	public class BehaviouralObjectVO extends ObjectVO
	{
		public function BehaviouralObjectVO()
		{
			super();
		}
		
		private var _displayStartTime:Number;
		public function get displayStartTime():Number
		{
			return _displayStartTime;
		}
		public function set displayStartTime(param:Number):void
		{
			_displayStartTime = param;
		}
		
		private var _displayEndTime:Number;
		public function get displayEndTime():Number
		{
			return _displayEndTime;
		}
		public function set displayEndTime(param:Number):void
		{
			_displayEndTime = param;
		}
		
		public function transferInfo(transferObj:ObjectVO):void {
			var obj:Object =  ObjectUtil.getClassInfo(transferObj);
			for each (var propName:String in transferObj.properties) {
				this[propName] = transferObj[propName];	
			}
		}
	}
}