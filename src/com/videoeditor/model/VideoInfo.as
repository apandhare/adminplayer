package com.videoeditor.model
{
	import mx.collections.ArrayCollection;
	
	import org.robotlegs.mvcs.Actor;
	
	public class VideoInfo extends Actor
	{
		public function VideoInfo()
		{
			super();
			
			videoId = 1;
			videoUrl = "assets/myfile_8.mp4";
			
			
		}
		
		public var videoId:Number;
		public var videoUrl:String;
		
		private var _originalXML:XML;
		public function set originalXML(value:XML):void {
			_originalXML = value;
		}
		
		
		public function get originalXML():XML {
			return _originalXML;
		}
		
		private var _data:XML;
		public function set data(value:XML):void {
			_data = value;
		}
		
		
		public function get data():XML {
			return _data;
		}
		
		private var _annotationsInfo:ArrayCollection;
		public function set annotationsInfo(value:ArrayCollection):void {
			_annotationsInfo = value;
		}
		
		
		public function get annotationsInfo():ArrayCollection {
			return _annotationsInfo;
		}
		
		public var currentPausePosition:Number;
		public var currentPositionNode:Object;
		public var pausedTime:int;
	}
}