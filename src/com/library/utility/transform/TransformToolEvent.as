package com.library.utility.transform
{
	import flash.events.Event;
	
	public class TransformToolEvent extends Event
	{
		public static const OBJECT_RESIZED:String = "resizeComplete";
		public static const OBJECT_ROTATED:String = "rotationComplete";
		public static const OBJECT_SKEWED:String = "skewComplete";
		public static const OBJECT_REGISTRATION_POINT_CHANGE:String = "registrationPointChange";
		public static const OBJECT_MOVED:String = "moveComplete";
		
		public function TransformToolEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		private var _data:Object;
		public function set data(value:Object):void {
			_data = value;
		}
		public function get data():Object {
			return _data;
		}
		
		private var _rotation:Number;
		public function set rotation(value:Number):void {
			_rotation = value;
		}
		public function get rotation():Number {
			return _rotation;
		}
		
		private var _scaleX:Number;
		public function set scaleX(value:Number):void {
			_scaleX = value;
		}
		public function get scaleX():Number {
			return _scaleX;
		}
		
		private var _scaleY:Number;
		public function set scaleY(value:Number):void {
			_scaleY = value;
		}
		public function get scaleY():Number {
			return _scaleY;
		}
		private var _x:Number;
		public function set x(value:Number):void {
			_x = value;
		}
		public function get x():Number {
			return _x;
		}
		
		private var _y:Number;
		public function set y(value:Number):void {
			_y = value;
		}
		public function get y():Number {
			return _y;
		}
	}
}