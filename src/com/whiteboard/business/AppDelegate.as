package com.whiteboard.business
{
	import com.whiteboard.AppConstants;
	
	import core.helpers.serviceUtils.ServiceBase;
	
	import mx.rpc.AsyncToken;
	import mx.rpc.IResponder;
	import mx.rpc.http.HTTPService;
	import mx.rpc.remoting.RemoteObject;

	public class AppDelegate implements IAppDelegate
	{
		private var service:HTTPService;
		private var remoteService:RemoteObject;
		
		public function getConfigData(responder:IResponder):void {
			/*remoteService = ServiceBase.getInstance().getRemoteObject('service');
			var token:AsyncToken = remoteService.getRegions(appTypeID);
			token.addResponder(responder);*/
			
			service = ServiceBase.getInstance().getHTTPService('service');
			service.url = service.rootURL + AppConstants.CONFIG_FILENAME;			
			var token:AsyncToken = service.send();
			token.addResponder(responder);
		}
		
	}
}