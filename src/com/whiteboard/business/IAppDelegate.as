package com.whiteboard.business
{
	import mx.collections.ArrayCollection;
	import mx.rpc.IResponder;

	public interface IAppDelegate
	{
		function getConfigData(responder:IResponder):void;
	}
}