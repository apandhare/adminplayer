package com.whiteboard.events
{
	import flash.events.Event;
	
	import mx.core.IDataRenderer;
	
	public class WhiteboardEvent extends Event
	{
		public static const STAGE_MOUSE_DOWN:String = "onMouseDownonStage";
		public static const TOOL_SELECTED:String = "toolSelected";
		public static const SHAPE_DRAW:String = "autoShapeDraw";
		public static const SET_SELECTION_TOOL:String = "setSelectionTool";
		public static const DRAW_INITIATED:String = "drawingInitiated";
		public static const DRAW_COMPLETE:String = "drawingComplete";
		public static const DELETE_OBJECT:String = "deleteObject";
		public static const DELETE_ALL_OBJECTS:String = "deleteAllObjects";
		public static const DISPLAY_OBJECT:String = "makeObjectVisible";
		public static const OBJECT_SELECTABLE:String = "makeObjectSelectableByTool";
		public static const APPLY_TRANSFORMATION:String = "applyAvailableTransformations";
		public static const UNDO:String = "undoLastAction";
		public static const OBJECT_SELECTED:String = "objectSelected";
		public static const OBJECT_ADDED_TO_STAGE:String = "onObjectAddedToStage";
		public static const ADD_BUTTON:String = "addCustomButton";
		public static const ADD_NOTE:String = "addNote";
		public static const ADD_IMAGE:String = "addImage";
		
		public function WhiteboardEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		private var _data:Object;
		public function set data(value:Object):void {
			_data = value;
		}
		public function get data():Object {
			return _data;
		}
		
		private var _selectable:Boolean;
		public function set selectable(value:Boolean):void {
			_selectable = value;
		}
		public function get selectable():Boolean {
			return _selectable;
		}
	}
}