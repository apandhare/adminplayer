package com.whiteboard.controller
{
	import com.whiteboard.business.IAppDelegate;
	import com.whiteboard.model.ConfigModel;
	import com.whiteboard.signals.ConfigLoadFailSignal;
	import com.whiteboard.signals.ConfigLoadSuccessSignal;
	
	import core.library.logger.CustomLogger;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.DateField;
	import mx.rpc.IResponder;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	public class GetConfigDataCommand extends SignalCommand implements IResponder
	{
		[Inject]
		public var configLoadSuccess:ConfigLoadSuccessSignal;
		
		[Inject]
		public var configLoadFailure:ConfigLoadFailSignal;
		
		[Inject]
		public var service:IAppDelegate;
		
		[Inject]
		public var configModel:ConfigModel;
		
		
		public function GetConfigDataCommand()
		{
			super();
		}
		
		override public function execute():void
		{
			CustomLogger.getInstance().sendMessage("GetConfigDataCommand - execute method entered");
			service.getConfigData(this);
		}
		
		public function result(data:Object):void
		{
//			CustomLogger.getInstance().sendMessage("\n\n" + (data.result as XML).toString());
			configModel.data = data.result;
			configLoadSuccess.dispatch();
		}
		
		public function fault(info:Object):void
		{
			//configLoadFailure.dispatch();
			Alert.show("Failed to load XML data.");
		}
	}
}