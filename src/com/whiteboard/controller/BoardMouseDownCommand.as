package com.whiteboard.controller
{
	import com.whiteboard.Properties;
	import com.whiteboard.TypeMapping;
	import com.whiteboard.business.IAppDelegate;
	import com.whiteboard.events.WhiteboardEvent;
	import com.whiteboard.model.SelectedToolModel;
	import com.whiteboard.model.vo.ObjectVO;
	
	import core.helpers.events.BaseEventDispatcher;
	import core.library.logger.CustomLogger;
	import core.library.utility.Utility;
	
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import mx.controls.Alert;
	import mx.rpc.IResponder;
	import mx.utils.StringUtil;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	public class BoardMouseDownCommand extends SignalCommand
	{
		[Inject]
		public var service:IAppDelegate;
		
		[Inject]
		public var objectSelection:SelectedToolModel;
		
		[Inject]
		public var parentInstance:Object;
		[Inject]
		public var downCoordinates:Point;

		[Inject]
		public var info:ObjectVO;
		
		[Inject]
		public var manualAdd:Boolean;
		
		public function BoardMouseDownCommand()
		{
			super();
		}
		
		override public function execute():void
		{
			CustomLogger.getInstance().sendMessage("BoardMouseDownCommand - execute method entered    " + objectSelection.type  + "    " + downCoordinates.x + "    " + downCoordinates.y);

//			CustomLogger.getInstance().sendMessage("BoardMouseDownCommand - execute method entered    " + objectSelection.tool);
			if(StringUtil.trim(info.type) != "") {
				Properties.dataExchanger = info;
			} else {
				Properties.dataExchanger = createInformationObject(parentInstance, downCoordinates.x, downCoordinates.y);
				Properties.dataExchanger.type = objectSelection.type;
			}
			objectSelection.tool = Utility.getClassDefination(new TypeMapping()[Properties.dataExchanger.type]);
//			SelectionController.getInstance().selectedInstance = instantiateShapeClass(Properties.dataExchanger);
			var instance:Object = instantiateShapeClass(Properties.dataExchanger);
//			CustomLogger.getInstance().sendMessage("I land up here ---   " + instance._model.name);
			if(manualAdd) 
				SelectionController.getInstance().selectedInstance = instance;
			
			
			var evtObj:WhiteboardEvent = new WhiteboardEvent(WhiteboardEvent.OBJECT_ADDED_TO_STAGE);
			evtObj.data = instance._model;
			BaseEventDispatcher.getInstance().dispatchEvent(evtObj);
			
			if(!instance.isDrawable) {
				var evtObj:WhiteboardEvent = new WhiteboardEvent(WhiteboardEvent.DRAW_COMPLETE);
				evtObj.data = instance._model;
				BaseEventDispatcher.getInstance().dispatchEvent(evtObj);
			}
		}
		

		/**
		 * 		Function which stores default properties of any 
		 * 		object drawn on stage like color,lineThickness etc
		 */
		public function createInformationObject(target:Object, initialX:Number, initialY:Number):ObjectVO
		{
			var infoObject:ObjectVO = new ObjectVO();
			infoObject.type = objectSelection.type;
			CustomLogger.getInstance().sendMessage("drawtype  --  " + objectSelection.type);
//			infoObject.oParent = target;
//			infoObject.name = objectSelection.tool.name;
			
			infoObject.initialX = initialX;
			infoObject.initialY = initialY;
			infoObject.scaleX = 1;
			infoObject.scaleY = 1;
			
			infoObject.fillColor = Properties.selFillColor;
			infoObject.fillAlpha = Properties.selFillAlpha;
			infoObject.lineColor = Properties.selLineColor;
			infoObject.lineAlpha = Properties.selLineAlpha;
			
			infoObject.linethickness = Properties.selLineThickness;
			infoObject.textColor = Properties.selTextColor;
			infoObject.textAlpha = Properties.textAlpha;
			infoObject.textSize = Properties.textSize;
			infoObject.textFamily = Properties.selFont;
			infoObject.alpha = Properties.alpha;
			
			infoObject.filters = new Array();
			infoObject.finalX = 0;
			infoObject.finalY = 0;
			
			if(objectSelection.type == 'note' || objectSelection.type == 'button') {
				infoObject.width = 175;
				infoObject.height = 45;
			} else {
				infoObject.text = "";
			}
			
			if(objectSelection.type == 'image') {
				infoObject.width = infoObject.height = 50;
			}
			
			return infoObject;
		}
		
		/**
		 * 		This function registers all the event handlers for the drawn object
		 * 		and also indicate parent of the object
		 * 		Also, sets the callback function of the object drawn
		 * 
		 * 		oInformation ->
		 * 		It contains all the data for the repository which
		 * 		helps to send it to some DB/FDS
		 */
		public function instantiateShapeClass(oInformation:Object):Object
		{
			CustomLogger.getInstance().sendMessage("I entered " + objectSelection.tool );
			var drawInstance:* = objectSelection.tool;
			parentInstance.addChild(drawInstance);
			
			if(!oInformation.name)
			oInformation.name = oInformation.type + drawInstance.name;
			
			drawInstance.buttonMode = true;

			Utility.setCallBack(drawInstance.draw);
			Utility.callFunction(oInformation);
//			CustomLogger.getInstance().sendMessage("I land up here ---   ");
			return drawInstance;
		}
		
		
	}
}