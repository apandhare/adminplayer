/**
 * Wrapper class to draw the selection Square/Rectangle, which takes care of the
 * data to be associated with it and applying them to it.
 * 
 * @author  Amol Pandhare
 */
package com.whiteboard.controller
{
	import com.library.utility.transform.TransformTool;
	import com.whiteboard.events.WhiteboardEvent;
	
	import core.helpers.events.BaseEventDispatcher;
	import core.library.logger.CustomLogger;
	import core.library.utility.UtilContextMenu;
	
	import flash.display.DisplayObject;
	
	import mx.containers.Canvas;
	
	import spark.components.BorderContainer;
	import spark.filters.GlowFilter;
	
	public class SelectionController 
	{
		private static var clsReference:SelectionController;
		[Bindable]
		public var isSelectionToolActive:Boolean;
		[Bindable]
		private var selectedObject:Object;
		
		public var tool:TransformTool = new TransformTool();
		private var _drawingArea:Object;
		
		public function addTool():void {
			_drawingArea.area.addChild(tool);
			tool.target = null;
		}
		public function set drawingArea(value:Object):void
		{
			_drawingArea = value;
			tool.skewEnabled = false;
			tool.rotationEnabled = false;
			tool.moveEnabled = true;
			addTool();
			CustomLogger.getInstance().sendMessage(tool);
		}
		public function get drawingArea():Object
		{
			return _drawingArea;
		}

		public static function getInstance():SelectionController
		{
			if (clsReference == null)
			{
				clsReference = new SelectionController();
			}
			
			return clsReference;
		}
		
		public function SelectionController()
		{
			
		}
		
		[Bindable]
		public function set selectedInstance(instance:Object):void
		{
			if (selectedObject && selectedObject._model.type == 'button')
				selectedObject.filters = [];
			if (selectedObject && selectedObject._model.type == 'image')
				tool.target = null;
			
			selectedObject = instance;
			if(selectedObject) {
//				CustomLogger.getInstance().sendMessage("##########################      "+selectedObject + "                 " + selectedObject._model.type);
			
				var evtObj:WhiteboardEvent = new WhiteboardEvent(WhiteboardEvent.OBJECT_SELECTED);
				evtObj.data = selectedObject._model;
				BaseEventDispatcher.getInstance().dispatchEvent(evtObj);
				
				if(selectedObject._model.type == 'button') {
					selectedObject.filters = [new GlowFilter(0xFF0000,.65,25,25)];
				}
				if(selectedObject._model.type == 'image') {
					tool.target = selectedObject as DisplayObject;
				}
			}
		}

		public function get selectedInstance():Object
		{
			return selectedObject;
		}
	}
}