package com.whiteboard
{
	public class Utility
	{
		public function Utility()
		{
		}
		
		private static var functionToCall:Function;
		public static function setCallBack(fnCall:Function):void
		{
			functionToCall = fnCall;
		}
		
		/**
		 *		Function which makes a call to the registered callback method
		 */
		public static function callFunction(dataCarrier:Object = null):Boolean
		{
			if (functionToCall == null)
			{
				return false;
			}
			else
			{
				functionToCall.call(null, dataCarrier);
				return true;
			}
		}
		
		public static var secondsPerMinute = 60;
		public static var minutesPerHour = 60;
		public static function convertSecondsToHHMMSS(intSecondsToConvert) {
			var hours = convertHours(intSecondsToConvert);
			var minutes = getRemainingMinutes(intSecondsToConvert);
			minutes = (minutes == 60) ? "00" : minutes;
			var seconds = getRemainingSeconds(intSecondsToConvert);
//			var miliseconds = intSecondsToConvert.toString().split('.')[1];
			
			if(hours.toString().length == 1)
				hours = "0" + hours.toString();
			if(minutes.toString().length == 1)
				minutes = "0" + minutes.toString();
			if(seconds.toString().length == 1)
				seconds = "0" + seconds.toString();
//			if(miliseconds.toString().length == 1)
//				miliseconds = "00" + miliseconds.toString();
//			if(miliseconds.toString().length == 2)
//				miliseconds = "0" + miliseconds.toString();
			
			return hours+" : "+minutes+" : "+seconds;
		}
		public static function convertHours(intSeconds) {
			var minutes = convertMinutes(intSeconds);
			var hours = Math.floor(minutes/minutesPerHour);
			return hours;
		}
		public static function convertMinutes(intSeconds) {
			return Math.floor(intSeconds/secondsPerMinute);
		}
		public static function getRemainingSeconds(intTotalSeconds) {
			return Math.round(intTotalSeconds%secondsPerMinute);
		}
		public static function getRemainingMinutes(intSeconds) {
			var intTotalMinutes = convertMinutes(intSeconds);
			return (intTotalMinutes%minutesPerHour);
		}
	}
}