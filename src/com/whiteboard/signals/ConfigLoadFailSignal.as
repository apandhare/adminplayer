package com.whiteboard.signals
{
	import org.osflash.signals.ISignal;
	import org.osflash.signals.Signal;
	
	public class ConfigLoadFailSignal extends Signal implements ISignal
	{
		public function ConfigLoadFailSignal()
		{
			super();
		}
	}
}