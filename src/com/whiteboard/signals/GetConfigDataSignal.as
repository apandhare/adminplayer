package com.whiteboard.signals
{
	import org.osflash.signals.ISignal;
	import org.osflash.signals.Signal;
	
	public class GetConfigDataSignal extends Signal implements ISignal
	{
		public function GetConfigDataSignal()
		{
			super();
		}
	}
}