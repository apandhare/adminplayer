package com.whiteboard.signals
{
	import org.osflash.signals.ISignal;
	import org.osflash.signals.Signal;
	
	public class ConfigLoadSuccessSignal extends Signal implements ISignal
	{
		public function ConfigLoadSuccessSignal()
		{
			super();
		}
	}
}