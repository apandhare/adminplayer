package com.whiteboard.signals
{
	import com.whiteboard.model.vo.ObjectVO;
	
	import flash.display.DisplayObject;
	import flash.geom.Point;
	
	import org.osflash.signals.ISignal;
	import org.osflash.signals.Signal;
	
	public class BoardMouseDownSignal extends Signal implements ISignal
	{
		public function BoardMouseDownSignal()
		{
			super(Object, Point, ObjectVO, Boolean);
		}
	}
}