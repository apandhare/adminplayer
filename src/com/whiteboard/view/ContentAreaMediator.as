package com.whiteboard.view
{
	import com.library.utility.transform.TransformTool;
	import com.whiteboard.AppConstants;
	import com.whiteboard.Properties;
	import com.whiteboard.controller.SelectionController;
	import com.whiteboard.events.WhiteboardEvent;
	import com.whiteboard.model.SelectedToolModel;
	import com.whiteboard.model.vo.ObjectVO;
	import com.whiteboard.signals.BoardMouseDownSignal;
	
	import core.helpers.events.BaseEventDispatcher;
	import core.library.logger.CustomLogger;
	import core.library.utility.Utility;
	import core.library.utility.transform.TransformTool;
	
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import mx.core.IVisualElement;
	import mx.core.UIComponent;
	
	import org.robotlegs.core.IMediator;
	import org.robotlegs.mvcs.Mediator;
	
	import spark.components.Group;

	public class ContentAreaMediator extends Mediator implements IMediator
	{
		public function ContentAreaMediator()
		{
			super();
		}
		
		[Inject]
		public var view:ContentArea;
		
		[Inject]
		public var objectSelection:SelectedToolModel;
		
		[Inject]
		public var mouseDownSignal:BoardMouseDownSignal;
		
		override public function onRegister():void {
			CustomLogger.getInstance().sendMessage("ContentAreaMediator - got registered \n");

			properties = Properties;
			
			SelectionController.getInstance().drawingArea = view;
			
			BaseEventDispatcher.getInstance().addEventListener(WhiteboardEvent.UNDO, stepBack);
			BaseEventDispatcher.getInstance().addEventListener(WhiteboardEvent.DELETE_OBJECT, deleteObject);
			BaseEventDispatcher.getInstance().addEventListener(WhiteboardEvent.DELETE_ALL_OBJECTS, clearBoardArea);
			
			addViewListener(MouseEvent.MOUSE_DOWN, onStageMouseDown);
			addViewListener(MouseEvent.MOUSE_MOVE, onStageMouseMove);
			addViewListener(MouseEvent.MOUSE_UP, onStageMouseUp);
		}
		
		private function stepBack(event:WhiteboardEvent):void {
			
			if(view.area.numChildren > 1 && (!(view.area.getChildAt(view.area.numChildren - 1) is com.library.utility.transform.TransformTool))) {
				view.area.removeChildAt(view.area.numChildren - 1);
			} else {
				view.area.removeChildAt(view.area.numChildren - 2);
			}
			if(SelectionController.getInstance().tool.target != null)
				SelectionController.getInstance().tool.target = null;
		}
		
		private function onToolMouseDown(event:MouseEvent):void {
			event.currentTarget.startDrag(false, new Rectangle(0,0,(view.width - event.currentTarget.width),(view.height - event.currentTarget.height)));
		}
		
		private var xPosOnMouseDown:Number;
		private var yPosOnMouseDown:Number;
		private var xPosOnMouseUp:Number;
		private var yPosOnMouseUp:Number;
		private var mouseDown:Boolean = false;
		private var properties:Object;
		
		private var dummyObject:Group = new Group();
		
		private function onStageMouseDown(evt:MouseEvent):void {
			CustomLogger.getInstance().sendMessage("MOUSEDOWN ON DRAWING AREA");
			BaseEventDispatcher.getInstance().dispatchEvent(new WhiteboardEvent(WhiteboardEvent.STAGE_MOUSE_DOWN));
			CustomLogger.getInstance().sendMessage("I got value   " + objectSelection.fullyQualifiedClassPath + "     " + objectSelection.type);
			if(objectSelection.type && objectSelection.type != AppConstants.SELECTION_TOOL) {
				if(objectSelection.type == 'button' || objectSelection.type == 'note' || objectSelection.type == 'image') {
					
				} else {
				if (!(mouseDown))
				{
					xPosOnMouseDown = evt.currentTarget.mouseX;
					yPosOnMouseDown = evt.currentTarget.mouseY;
					mouseDown = true;
					
					var pt:Point = new Point(xPosOnMouseDown, yPosOnMouseDown);
					mouseDownSignal.dispatch(view.area, pt, new ObjectVO, true);
					
					BaseEventDispatcher.getInstance().dispatchEvent(new WhiteboardEvent(WhiteboardEvent.DRAW_INITIATED));
				}
				}
			}
		}
		
		private function onStageMouseMove(evt:MouseEvent):void {
			if(objectSelection.type && objectSelection.type != AppConstants.SELECTION_TOOL) {
				if (mouseDown)
				{
					xPosOnMouseUp = evt.currentTarget.mouseX - xPosOnMouseDown;
					yPosOnMouseUp = evt.currentTarget.mouseY - yPosOnMouseDown;

					triggerCall();
				}
			}
		}
		
		private function onStageMouseUp(evt:MouseEvent):void {
			if(objectSelection.type && objectSelection.type != AppConstants.SELECTION_TOOL) {
				if (mouseDown)
				{
					xPosOnMouseUp = evt.currentTarget.mouseX - xPosOnMouseDown;
					yPosOnMouseUp = evt.currentTarget.mouseY - yPosOnMouseDown;
					mouseDown = false;
				
					triggerCall();
					objectSelection.tool = null;
				}
				CustomLogger.getInstance().sendMessage("\n--------------  " + view.area.numChildren);
				CustomLogger.getInstance().sendMessage("\n--------------  " + SelectionController.getInstance().selectedInstance.isDrawable);
				
				if(SelectionController.getInstance().selectedInstance.isDrawable) {
				if(objectSelection.type == 'note')
					BaseEventDispatcher.getInstance().dispatchEvent(new WhiteboardEvent(WhiteboardEvent.SET_SELECTION_TOOL));
				
				var evtObj:WhiteboardEvent = new WhiteboardEvent(WhiteboardEvent.DRAW_COMPLETE);
				if(objectSelection.type == 'freehand')
					SelectionController.getInstance().selectedInstance._model.auto = true;
				evtObj.data = SelectionController.getInstance().selectedInstance._model;
				BaseEventDispatcher.getInstance().dispatchEvent(evtObj);
				
				SelectionController.getInstance().tool.target = SelectionController.getInstance().selectedInstance as DisplayObject;
				var selectedTarget:Object = SelectionController.getInstance().tool.target as Object;

				if(objectSelection.type != 'freehand' && objectSelection.type != 'triangle') {
					if(selectedTarget._model.finalX < 0 && selectedTarget._model.finalY < 0) {
						//quadrant 4 -- height and width negative
						SelectionController.getInstance().tool.registration = new Point(selectedTarget.x - selectedTarget.width/2, selectedTarget.y - selectedTarget.height/2);
					} else if(selectedTarget._model.finalX > 0 && selectedTarget._model.finalY < 0) {
						//quadrant 1 -- height negative and width positive
						SelectionController.getInstance().tool.registration = new Point(selectedTarget.x + selectedTarget.width/2, selectedTarget.y - selectedTarget.height/2);
					} else if(selectedTarget._model.finalX < 0 && selectedTarget._model.finalY > 0) {
						//quadrant 3 -- height positive and width negative
						SelectionController.getInstance().tool.registration = new Point(selectedTarget.x - selectedTarget.width/2, selectedTarget.y + selectedTarget.height/2);
					} else {
						//quadrant 2 -- height and width positive
						SelectionController.getInstance().tool.registration = new Point(selectedTarget.x + selectedTarget.width/2, selectedTarget.y + selectedTarget.height/2);
					}
				}
//				SelectionController.getInstance().selectedInstance.setValues();
				SelectionController.getInstance().tool.target = null;
//				SelectionController.getInstance().selectedInstance = null;
				}
			}
			
		}
		
		public function triggerCall(isFDSEnabled:Boolean = false):void
		{
//			CustomLogger.getInstance().sendMessage("Entering the method where things are finally sent");
			properties.dataExchanger.oManipulateMove = mouseDown;
			properties.dataExchanger.finalX = xPosOnMouseUp;
			properties.dataExchanger.finalY = yPosOnMouseUp;
			
			Utility.callFunction(properties.dataExchanger);
		}
		
		public function deleteObject(evt:WhiteboardEvent):void {
//			view.removeElement(evt.data as IVisualElement);
			view.area.removeChild(evt.data as DisplayObject);
			SelectionController.getInstance().selectedInstance = null;
			SelectionController.getInstance().tool.target = null;
//			BaseEventDispatcher.getInstance().dispatchEvent(new WhiteboardEvent(WhiteboardEvent.SET_SELECTION_TOOL));
//			CustomLogger.getInstance().sendMessage(this.numChildren);
		}
		
		public function clearBoardArea(evt:WhiteboardEvent):void {
			view.area.removeChildren();
			SelectionController.getInstance().addTool();
//			view.removeAllElements();
		}
	}
}