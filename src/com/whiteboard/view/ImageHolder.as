package com.whiteboard.view
{
	import com.whiteboard.Properties;
	import com.whiteboard.controller.SelectionController;
	import com.whiteboard.model.vo.ObjectVO;
	import com.whiteboard.view.baseContainers.ObjectHolder;
	
	import core.library.logger.CustomLogger;
	
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.geom.Point;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.utils.setTimeout;

	public class ImageHolder extends ObjectHolder
	{
		public function ImageHolder()
		{
			super();
			isDrawable = false;
		}
		
		//function that applies values, transformation based on the vale object recieved
		public override function draw(objectInfo:ObjectVO):void {
			_model = objectInfo;
			
			x = _model.initialX;
			y = _model.initialY;
			
			var image:String;
			if(_model.hasOwnProperty('imageUrl')) {
				CustomLogger.getInstance().sendMessage("Loading existing url as Image");
				image = _model.imageUrl;
			} else {
				image = Properties.imageUrl;
			}
			CustomLogger.getInstance().sendMessage(image);
			loadImage(image);
		}
		
		var loader:Loader;
		var loadingHolder:Sprite;
		
		//loads image based on the url provided
		private function loadImage(url:String):void {
			loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, loadBytesHandler);
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onLoadError);
			loader.load(new URLRequest(url));
			addChild(loader);
			
			//image loading indicator code
			loadingHolder = new Sprite();
			var txt:TextField = new TextField();
			txt.text = "Loading...";
			loadingHolder.addChild(txt);
			addChild(loadingHolder);
			loadingHolder.graphics.beginFill(0xFFFFFF);
			loadingHolder.graphics.drawRoundRect(0,0,75,20,5);
			loadingHolder.graphics.endFill();
		}
		
		//triggered incase there is error loading the image
		private function onLoadError(event:IOErrorEvent):void {
			CustomLogger.getInstance().sendMessage("############ ERROR LOADING BYTES ################");
		}
		
		//triggered on the image load complete
		private function loadBytesHandler(event:Event):void
		{
			_model.originalHeight = loader.height;
			_model.originalWidth = loader.width;
			CustomLogger.getInstance().sendMessage("originalHeight  --  " + loader.height);
			CustomLogger.getInstance().sendMessage("originalWidth  --  " + loader.width);
			
			var loaderInfo:LoaderInfo = (event.target as LoaderInfo);
			loaderInfo.removeEventListener(Event.COMPLETE, loadBytesHandler);
			
			if(!_model.hasOwnProperty('imageUrl')) {
				setTimeout(setSelection, 250);
			}
			
			//apply transformation based on the information recieved
			_model.imageUrl = Properties.imageUrl;
			loader.height = _model.height;
			loader.width = _model.width;
			scaleX = _model.scaleX;
			scaleY = _model.scaleY;
			
			removeChild(loadingHolder);
		}
		
		//select the image object drawn and apply center point registeration to it
		private function setSelection():void {
			SelectionController.getInstance().tool.target = this;
			//set tool registration point to center here
			SelectionController.getInstance().tool.registration = new Point(this.x + loader.width/2, this.y + loader.height/2);
		}
	}
}
		
		
	
