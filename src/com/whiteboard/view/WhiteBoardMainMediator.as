package com.whiteboard.view
{
	import com.whiteboard.AppConstants;
	import com.whiteboard.InstancePool;
	import com.whiteboard.TypeMapping;
	import com.whiteboard.Utility;
	import com.whiteboard.controller.SelectionController;
	import com.whiteboard.events.WhiteboardEvent;
	import com.whiteboard.model.ConfigModel;
	import com.whiteboard.model.SelectedToolModel;
	import com.whiteboard.model.vo.ObjectVO;
	import com.whiteboard.signals.BoardMouseDownSignal;
	import com.whiteboard.signals.ConfigLoadFailSignal;
	import com.whiteboard.signals.ConfigLoadSuccessSignal;
	import com.whiteboard.signals.GetConfigDataSignal;
	
	import core.helpers.events.BaseEventDispatcher;
	import core.library.logger.CustomLogger;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.geom.Point;
	
	import mx.utils.StringUtil;
	
	import org.robotlegs.core.IMediator;
	import org.robotlegs.mvcs.Mediator;

	public class WhiteBoardMainMediator extends Mediator implements IMediator
	{
		public function WhiteBoardMainMediator()
		{
			super();
		}
		
		[Inject]
		public var view:WhiteBoardMain;
		
		[Inject]
		public var configSignal:GetConfigDataSignal;
		
		[Inject]
		public var configLoadSuccess:ConfigLoadSuccessSignal;
		
		[Inject]
		public var configLoadFailure:ConfigLoadFailSignal;
		
		[Inject]
		public var configModel:ConfigModel;
		
		[Inject]
		public var mouseDownSignal:BoardMouseDownSignal;
		
		[Inject]
		public var objectSelection:SelectedToolModel;
		
		private var dictionary:TypeMapping = new TypeMapping();
		
		//called when the view gets registered with the mediator... automatically called by the robotlegs framework
		//place where we register event listners and signals listener functions
		override public function onRegister():void {
//			BaseEventDispatcher.getInstance().dispatchEvent(new Event("WHITEBOARD_READY"));
			
			CustomLogger.getInstance().sendMessage("Application Initialized. ");
			CustomLogger.getInstance().sendMessage("WhiteBoardMainMediator - got registered \n");
			
			configLoadSuccess.add(onConfigLoad);
			configLoadFailure.add(onConfigFail);
			
			addViewListener(WhiteboardEvent.SHAPE_DRAW, dynamicShapeCreate);
//			addViewListener(WhiteboardEvent.APPLY_TRANSFORMATION, applyTransformationToShape);
//			addViewListener(WhiteboardEvent.SET_SELECTION_TOOL, setDefaultTool);
			
			BaseEventDispatcher.getInstance().addEventListener(WhiteboardEvent.TOOL_SELECTED, onToolSelection);
			BaseEventDispatcher.getInstance().addEventListener(WhiteboardEvent.DELETE_OBJECT, onDeleteObject);
			BaseEventDispatcher.getInstance().addEventListener(WhiteboardEvent.OBJECT_SELECTED, onObjectSelection);
			
			BaseEventDispatcher.getInstance().addEventListener(WhiteboardEvent.ADD_BUTTON, onButtonAdd);
			BaseEventDispatcher.getInstance().addEventListener(WhiteboardEvent.ADD_IMAGE, onImageAdd);
			
			InstancePool.instantiateLibrary();
			
//			configSignal.dispatch();
		}
		
		//triggered every time a new tool type is selected
		private function onToolSelection(evt:WhiteboardEvent):void {
			CustomLogger.getInstance().sendMessage("####################  Whiteboard ready to use  ##################");

//			if(view.showProperties) {
//				if(evt.data.type == AppConstants.SELECTION_TOOL)// || evt.data.type == 'note')
//					view.propertyWindow.visible = false;
//				else
//					view.propertyWindow.visible = true;
//			}
		}
		
		//function to instantiate a tool dynamically based on the type
		private function instantiateTool(toolType:String):void {
			objectSelection.fullyQualifiedClassPath = dictionary[StringUtil.trim(toolType)];
			
			var evtObj:WhiteboardEvent = new WhiteboardEvent(WhiteboardEvent.TOOL_SELECTED);
			evtObj.data = objectSelection;
			BaseEventDispatcher.getInstance().dispatchEvent(evtObj);
		}
		
		//function to instantiate a button tool and add it to stage
		private function onButtonAdd(evt:WhiteboardEvent):void {
			if(objectSelection.type != 'button') {
				objectSelection.type = 'button';
				instantiateTool(objectSelection.type);
			}
			
			
			var pt:Point = new Point(250, 50);
			mouseDownSignal.dispatch(view.scribbleArea.area, pt, new ObjectVO, true);
		}
		
		//function to instantiate a image tool and add it to stage
		private function onImageAdd(evt:WhiteboardEvent):void {
			if(objectSelection.type != 'image') {
				objectSelection.type = 'image';
				instantiateTool(objectSelection.type);
			}
			
			var pt:Point = new Point(250, 50);
			mouseDownSignal.dispatch(view.scribbleArea.area, pt, new ObjectVO, true);
			
			SelectionController.getInstance().tool.target = null;
		}
		
		//triggered when a object is deleted from the stage
		private function onDeleteObject(evt:WhiteboardEvent):void {
//			if(view.showProperties) {
//				view.propertyWindow.visible = false;
//			}
		}
		
		//triggered everytime a object like button or image is selected which is drawn on top of the video
		private function onObjectSelection(evt:WhiteboardEvent):void {
//			if(view.showProperties) {
//				view.propertyWindow.visible = true;
//			}
		}
		
		/*private function setDefaultTool():void {
			objectSelection.type = AppConstants.SELECTION_TOOL;
			objectSelection.fullyQualifiedClassPath = dictionary[StringUtil.trim(objectSelection.type)];
			objectSelection.tool = SelectionController.getInstance();
			
			var evtObj:WhiteboardEvent = new WhiteboardEvent(WhiteboardEvent.TOOL_SELECTED);
			evtObj.data = objectSelection;
			BaseEventDispatcher.getInstance().dispatchEvent(evtObj);
		}*/
		
		//creates a dynamic object from existing object information
		private function dynamicShapeCreate(evt:WhiteboardEvent):void {
			CustomLogger.getInstance().sendMessage("draw dynamic shape now");
			mouseDownSignal.dispatch(view.scribbleArea.area, new Point(evt.data.initialX, evt.data.initialY), evt.data, false);
		}
		
		/*private function applyTransformationToShape(evt:WhiteboardEvent):void {
			SelectionController.getInstance().selectedInstance.applyTransformation();
		}*/
		
		private function onConfigLoad():void {
//			CustomLogger.getInstance().sendMessage("\n\n" + configModel.data.toString());
//			view.tools = configModel.data.category.tool;
		}

		private function onConfigFail():void {
			
		}
	}
}