package com.whiteboard.model.vo
{
	import core.library.logger.CustomLogger;
	import core.library.utility.Utility;
	
	import flash.xml.XMLDocument;
	import flash.xml.XMLNode;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.xml.SimpleXMLDecoder;
	import mx.utils.ArrayUtil;
	import mx.utils.ObjectUtil;
	import mx.utils.XMLUtil;

	[Bindable]
	dynamic public class ObjectVO
	{
		private var _name:String;
		public function get name():String
		{
			return _name;
		}
		public function set name(param:String):void
		{
			_name = param;
		}
		
		private var _type:String;
		public function get type():String
	    {
	    	return _type;
	    }
       	public function set type(param:String):void
       	{
       		_type = param;
       	}
       	
       	
		private var _height:Number;
		public function get height():Number
	    {
	    	return _height;
	    }
       	public function set height(param:Number):void
       	{
       		_height = param;
       	}
       	
       	
		private var _width:Number;
		public function get width():Number
	    {
	    	return _width;
	    }
       	public function set width(param:Number):void
       	{
       		_width = param;
       	}
       	
       	
		private var _rotation:Number;
		public function get rotation():Number
	    {
	    	return _rotation;
	    }
       	public function set rotation(param:Number):void
       	{
       		_rotation = param;
       	}
       	
       	
		private var _index:Number;
		public function get index():Number
	    {
	    	return _index;
	    }
       	public function set index(param:Number):void
       	{
       		_index = param;
       	}
       	
       	
		private var _linethickness:Number;
		public function get linethickness():Number
	    {
	    	return _linethickness;
	    }
       	public function set linethickness(param:Number):void
       	{
       		_linethickness = param;
       	}
		
		
		private var _initialX:Number;
		public function get initialX():Number
	    {
	    	return _initialX;
	    }
       	public function set initialX(param:Number):void
       	{
       		_initialX = param;
       	}
       	
       	
		private var _initialY:Number;
		public function get initialY():Number
	    {
	    	return _initialY;
	    }
       	public function set initialY(param:Number):void
       	{
       		_initialY = param;
       	}
       	
       	
		private var _finalX:Number;
		public function get finalX():Number
	    {
	    	return _finalX;
	    }
       	public function set finalX(param:Number):void
       	{
       		_finalX = param;
       	}
       	
       	
		private var _finalY:Number;
		public function get finalY():Number
	    {
	    	return _finalY;
	    }
       	public function set finalY(param:Number):void
       	{
       		_finalY = param;
       	}
		
		private var _filters:Array;
		public function get filters():Array
	    {
	    	return _filters;
	    }
       	public function set filters(param:Array):void
       	{
       		_filters = param;
       	}
       	
		
		private var _fillAlpha:Number;
		public function get fillAlpha():Number
		{
			return _fillAlpha;
		}
		public function set fillAlpha(param:Number):void
		{
			_fillAlpha = param;
		}
		
		private var _fillColor:uint;
		public function get fillColor():uint
	    {
	    	return _fillColor;
	    }
       	public function set fillColor(param:uint):void
       	{
       		_fillColor = param;
       	}
		
		
		private var _lineColor:uint;
		public function get lineColor():uint
	    {
	    	return _lineColor;
	    }
       	public function set lineColor(param:uint):void
       	{
       		_lineColor = param;
       	}
       	

		private var _lineAlpha:Number;
		public function get lineAlpha():Number
		{
			return _lineAlpha;
		}
		public function set lineAlpha(param:Number):void
		{
			_lineAlpha = param;
		}
		
		private var _oManipulateMove:Boolean;
		public function get oManipulateMove():Boolean
	    {
	    	return _oManipulateMove;
	    }
       	public function set oManipulateMove(param:Boolean):void
       	{
       		_oManipulateMove = param;
       	}
		
		
		private var _scaleX:Number;
		public function get scaleX():Number
		{
			return _scaleX;
		}
		public function set scaleX(param:Number):void
		{
			_scaleX = param;
		}
		
		
		private var _scaleY:Number;
		public function get scaleY():Number
		{
			return _scaleY;
		}
		public function set scaleY(param:Number):void
		{
			_scaleY = param;
		}
		
		private var _textColor:uint;
		public function get textColor():uint
		{
			return _textColor;
		}
		public function set textColor(param:uint):void
		{
			_textColor = param;
		}
		
		private var _textAlpha:Number;
		public function get textAlpha():Number
		{
			return _textAlpha;
		}
		public function set textAlpha(param:Number):void
		{
			_textAlpha = param;
		}
		
		private var _alpha:Number;
		public function get alpha():Number
		{
			return _alpha;
		}
		public function set alpha(param:Number):void
		{
			_alpha = param;
		}
		
		private var _textSize:Number;
		public function get textSize():Number
		{
			return _textSize;
		}
		public function set textSize(param:Number):void
		{
			_textSize = param;
		}
		
		private var _textFamily:String;
		public function get textFamily():String
		{
			return _textFamily;
		}
		public function set textFamily(param:String):void
		{
			_textFamily = param;
		}
		
		/*private var _showBorder:Boolean;
		public function get showBorder():Boolean
		{
			return _showBorder;
		}
		public function set showBorder(param:Boolean):void
		{
			_showBorder = param;
		}*/
		
		private var _text:String = "Enter Text here";
		public function get text():String
		{
			return _text;
		}
		public function set text(param:String):void
		{
			_text = param;
		}
		
		
		public var pointsCollection:ArrayCollection = new ArrayCollection();
		
		public function toString():String {
			return "\n\nInitial X Pos - " + initialX + 
				"\nInitial Y Pos - " + initialY +
				"\nFinal X Pos - " + finalX +
				"\nFinal Y Pos - " + finalY +
				"\nLine Thickness - " + linethickness +
				"\nLine Color - " + lineColor +
				"\nFill Color - " + fillColor +
				"\nIndex - " + index +
				"\nHeight - " + height +
				"\nWidth - " + width +
				"\nRotation - " + rotation +
				"\ntype - " + type +
				"\nX SCALE - " + scaleX +
				"\nY SCALE - " + scaleY
				;
		}
		
		public function getXMLformat():XML {
			var objProp:XML = <annotationObject/>;
//			objProp.name = this.oName;
//			objProp.properties = <properties/>;
//			CustomLogger.getInstance().sendMessage("Creating xml object " + this['oType']);
			var obj:Object =  ObjectUtil.getClassInfo(this);
			for each (var propName:String in obj.properties) {
//				CustomLogger.getInstance().sendMessage(propName + " = " + this[propName]);
				objProp[propName] = this[propName];
				if(this[propName] is ArrayCollection) {
//					CustomLogger.getInstance().sendMessage(Utility.objectToXML(this[propName]).toString());	
					objProp[propName] = Utility.objectToXML(this[propName]).source;
				} else {
					objProp[propName] = this[propName];
				}
			}
			
//			CustomLogger.getInstance().sendMessage("Creating xml object " + objProp.toString());
			return objProp;
		}
		
		public function transferInfo(transferObj:Object):void {
			var obj:Object =  ObjectUtil.getClassInfo(transferObj);
			for each (var propName:String in transferObj.properties) {
				this[propName] = transferObj[propName];	
			}
		}
	}
}