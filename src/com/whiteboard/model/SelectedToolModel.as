package com.whiteboard.model
{
	import com.whiteboard.model.vo.ObjectVO;
	
	import org.robotlegs.mvcs.Actor;
	
	public class SelectedToolModel extends Actor
	{
		private static var instance:SelectedToolModel;
		public function SelectedToolModel()
		{
			super();
			instance = this;
		}
		
		private var _tool:*;
		public function set tool(value:*):void {
			_tool = value;
		}
		
		
		public function get tool():* {
			return _tool;
		}
		
		public var type:String;
		public var fullyQualifiedClassPath:String;
		
		private var _data:ObjectVO;
		public function set data(value:ObjectVO):void {
			_data = value;
		}
		
		
		public function get data():ObjectVO {
			return _data;
		}
		
		public static function getInstance():SelectedToolModel {
			return instance;
		}
	}
}