package com.whiteboard.model
{
	import org.robotlegs.mvcs.Actor;
	
	public class ConfigModel extends Actor
	{
		public function ConfigModel()
		{
			super();
		}
		
		private var _data:Object;
		public function set data(value:Object):void {
			_data = value;
		}
		
		
		public function get data():Object {
			return _data;
		}
	}
}