/**
 * Class just used for the purpose of generating pool of instances
 * related to dynamically drawable objects, so that the classes could
 * dynamically be instantiated via an external XML.
 * 
 * @author  Amol Pandhare
 */
package com.whiteboard
{
	import com.whiteboard.view.CustomButton;
	import com.whiteboard.view.ImageHolder;
	
	public class InstancePool
	{
		/**
		 * Only instantiates below classes in the application memory pool.
		 */
		public static function instantiateLibrary():void
		{
			new ImageHolder;
			new CustomButton;
		}
	}
}