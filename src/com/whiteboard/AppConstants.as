package com.whiteboard
{
	import mx.core.UIComponent;

	public class AppConstants
	{
		public static var CONFIG_FILENAME:String = 'config.xml';
		public static var TOOL_FILENAME:String = 'ToolBox.xml';
		
		public static var SELECTION_TOOL:String = "selection";
		
		//this property tells the whiteboard to allow drawn objects to be selected and transformed
		[Bindable]
		public static var selectable:Boolean;
		[Bindable]
		public static var drawingArea:UIComponent;
	}
}