package com.whiteboard
{
	import com.whiteboard.model.vo.ObjectVO;

	public class Properties
	{
		public static var xPosOnMouseDown:Number;
		public static var yPosOnMouseDown:Number;
		public static var xPosOnMouseUp:Number;
		public static var yPosOnMouseUp:Number;
		
		public static var mouseDown:Boolean
		public static var dataExchanger:ObjectVO;
		public static var drawingStage:*;
		
		public static var imageUrl:String = "http://dev01.rzminc.com/admin player/images/Jellyfish.jpg";
//		public static var artBoard:BaseCanvas;
//		public static var tackBoard:BaseCanvas;
//		public static var backBoard:BaseCanvas;
		
		public static var fdsDestinationDefined:Boolean;
		
		public static var drawType;
		
		public static var selTextColor:uint = 0x336699;
		public static var selLineColor:uint = 0x336699;
		public static var selFillColor:uint = 0xFFFFFF;
		public static var selFillAlpha:Number = 1;
		public static var selLineAlpha:Number = 1;
		public static var selLineThickness:int = 2;
		public static var textSize:Number = 12;
		public static var alpha:Number = 1;
		public static var textAlpha:Number = 1;
		public static var selFont:String = "arial";
		
		
		public static var fdsEnabled:Boolean = false;
		public static var destination:String = "kanvas";
	}
}