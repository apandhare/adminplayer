package com.whiteboard.common
{
	import com.whiteboard.model.vo.ObjectVO;

	public interface IManageObjectMethods
	{
		function draw(oInformation:ObjectVO):void;
		
		function setFillColor(color:uint):void;
		function setBorderColor(color:uint):void;
		function setTextColor(color:uint):void;
		function setFillAlpha(alpha:Number):void;
		function setBorderAlpha(alpha:Number):void;
		function setTextAlpha(alpha:Number):void;
		function setBorderThickness(thickness:Number):void;
		function setFontSize(size:Number):void;
		function setFontFamily(family:String):void;
		function setAlpha(alpha:Number):void;
	}
}