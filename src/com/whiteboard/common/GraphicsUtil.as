package com.whiteboard.common
{
	import com.whiteboard.view.baseContainers.ObjectHolder;
	
	import flash.display.BitmapData;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	
	import spark.components.Group;
	
	public class GraphicsUtil extends ObjectHolder
	{
		/**
		* Sets Line style for a current graphic object.
		*/
	    public function lineStyle(nThickness:Number = 1, nRGB:Number = 0, nAlpha:Number = 1, bPixelHinting:Boolean = false, sScaleMode:String = "normal", sCaps:String = null, sJoints:String = null, nMiterLimit:Number = 3):void {
	    	this.graphics.lineStyle(nThickness, nRGB, nAlpha, bPixelHinting, sScaleMode, sCaps, sJoints, nMiterLimit);
	    }
	    
  		/**
		* sets line gradiant style for a current graphic object.
		*/
	    public function lineGradientStyle(sType:String, aColors:Array, aAlphas:Array, aRatios:Array, mtxTransform:Matrix = null, sMethod:String = "pad", sInterpolation:String = "rgb", nFocalPoint:Number = 0):void {
	    	this.graphics.lineGradientStyle(sType, aColors, aAlphas, aRatios, mtxTransform, sMethod, sInterpolation, nFocalPoint);
	    }

 		/**
		* Allows to fill the inner solid color of a current object like square, circle, etc.
		*/
	    public function beginFill(nRGB:Number, nAlpha:Number = 1):void {
	     	this.graphics.beginFill(nRGB, nAlpha);
	    }

 		/**
		* Allows to fill the inner gradient color of a current object like square, circle, etc.
		*/
	    public function beginGradientFill(sFillType:String, aColors:Array, aAlphas:Array, aRatios:Array, mtxTransform:Matrix = null, sMethod:String = "pad", sInterpolation:String = "rgb", nFocalPoint:Number = 0):void {
	     	this.graphics.beginGradientFill(sFillType, aColors, aAlphas, aRatios, mtxTransform, sMethod, sInterpolation, nFocalPoint);
	    }
	    
 		/**
		* Allows to fill the solid color to a bitmap object.
		*/
	    public function beginBitmapFill(bmpData:BitmapData, mtxTransform:Matrix = null, bRepeat:Boolean = true, bSmooth:Boolean = false):void {
	    	this.graphics.beginBitmapFill(bmpData, mtxTransform, bRepeat, bSmooth);
	    }
	
 		/**
		* specifies end of inner color fill for a current graphic object.
		*/
	    public function endFill():void {
	     	this.graphics.endFill();
	    }
	
 		/**
		* Clears the complete graphic drawn on a current container.
		*/
	    public function clear():void {
	      	this.graphics.clear();
	    }
	
 		/**
		* Moves the point coordinate to the specified location on a current container.
		*/
	    public function moveTo(nX:Number, nY:Number):void {
	      	this.graphics.moveTo(nX, nY);
	    }
	
 		/**
		* Draws a line to the specified location on a current container.
		*/
	    public function lineTo(nX:Number, nY:Number):void {
	      	this.graphics.lineTo(nX, nY);
	    }
	
 		/**
		* Draws a curve on a current container.
		*/
	    public function curveTo(nCtrlX:Number, nCtrlY:Number, nAnchorX:Number, nAnchorY:Number):void {
	    	this.graphics.curveTo(nCtrlX, nCtrlY, nAnchorX, nAnchorY);
	    }
	}
}