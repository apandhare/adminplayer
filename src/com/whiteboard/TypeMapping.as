package com.whiteboard
{
	import flash.utils.Dictionary;
	
	public class TypeMapping extends Dictionary
	{
		public var selection:String = 'com.whiteboard.controller.SelectionController';
	
		public var button:String = 'com.whiteboard.view.CustomButton';
		public var image:String = 'com.whiteboard.view.ImageHolder';
	}
}