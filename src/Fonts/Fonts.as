// ActionScript file

/* Available font list */
[Embed(source="BaroqueScript.ttf",
					fontName = "BaroqueScript",
					mimeType = "application/x-font")]
public var BaroqueScript:Class;

[Embed(source="Cake Nom.ttf",
					fontName = "CakeNom",
					mimeType = "application/x-font")]
public var CakeNom:Class;

[Embed(source="arial.ttf",
					fontName = "Arial Normal",
					mimeType = "application/x-font")]
public var ArialN:Class;

[Embed(source="arialbd.ttf",
					fontName = "Arial Bold",
					mimeType = "application/x-font")]
public var ArialB:Class;

[Embed(source="arialbi.ttf",
					fontName = "Arial Bold Italic",
					mimeType = "application/x-font")]
public var ArialBI:Class;

[Embed(source="ariali.ttf",
					fontName = "Arial Italic",				
					mimeType = "application/x-font")]
public var ArialI:Class;

[Embed(source="BebasNeue.otf",
					fontName = "BebasNeue",				
					mimeType = "application/x-font")]
public var BebasNeue:Class;

[Embed(source="impact.ttf",
					fontName = "Impact",				
					mimeType = "application/x-font")]
public var Impact:Class;

[Embed(source="Press Style.ttf",
					fontName = "Press Style",				
					mimeType = "application/x-font")]
public var PressStyle:Class;

[Embed(source="The Rainmaker.otf",
					fontName = "The Rainmaker",				
					mimeType = "application/x-font")]
public var TheRainmaker:Class;

[Embed(source="varsity_regular.ttf",
					fontName = "Varsity Regular",				
					mimeType = "application/x-font")]
public var Varsity:Class;

[Embed(source="verdana.ttf",
					fontName = "Verdana",
					mimeType = "application/x-font")]
public var Verdana:Class;

[Embed(source="verdanab.ttf",
					fontName = "Verdana Bold",
					mimeType = "application/x-font")]
public var VerdanaB:Class;

[Embed(source="verdanai.ttf",
					fontName = "Verdana Italic",
					mimeType = "application/x-font")]
public var VerdanaI:Class;

[Embed(source="verdanaz.ttf",
					fontName = "Verdana Z",
					mimeType = "application/x-font")]
public var VerdanaZ:Class;

[Embed(source="Chunkfive.otf",
						fontName = "Chunkfive",
						mimeType = "application/x-font")]
public var Chunkfive:Class;

[Embed(source="DroidSerif.ttf",
						fontName = "DroidSerif",
						mimeType = "application/x-font")]
public var DroidSerif:Class;

[Embed(source="DroidSerif-Bold.ttf",
							fontName = "DroidSerifBold",
							mimeType = "application/x-font")]
public var DroidSerifBold:Class;



[Embed(source="DroidSerif-BoldItalic.ttf",
						fontName = "DroidSerif Bold Italic",
						mimeType = "application/x-font")]
public var DroidSerifBoldItalic:Class;

[Embed(source="DroidSerif-Italic.ttf",
						fontName = "DroidSerif Italic",
						mimeType = "application/x-font")]
public var DroidSerifItalic:Class;

[Embed(source="HennyPenny-Regular.otf",
						fontName = "HennyPenny",
						mimeType = "application/x-font")]
public var HennyPennyRegular:Class;


[Embed(source="Pacifico.ttf",
						fontName = "Pacifico",				
						mimeType = "application/x-font")]
public var Pacifico:Class;


[Embed(source="Quicksand_Dash.otf",
						fontName = "Quicksand Dash",				
						mimeType = "application/x-font")]
public var QuicksandDash:Class;

[Embed(source="Quicksand-Bold.otf",
						fontName = "Quicksand Bold",				
						mimeType = "application/x-font")]
public var QuicksandBold:Class;

[Embed(source="Quicksand-BoldItalic.otf",
						fontName = "Quicksand BoldItalic",				
						mimeType = "application/x-font")]
public var QuicksandBoldItalic:Class;

[Embed(source="Quicksand-Italic.otf",
						fontName = "Quicksand Italic",				
						mimeType = "application/x-font")]
public var QuicksandItalic:Class;

[Embed(source="Quicksand-Light.otf",
						fontName = "Quicksand Light",				
						mimeType = "application/x-font")]
public var QuicksandLight:Class;



